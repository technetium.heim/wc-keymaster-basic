<?php
namespace Keymaster;

use Auth;
use Route;

class Keymaster
{
	protected $remote_group_key;

	public function makeAuthRoutes( $remote_group_key ,  $namespace = "Keymaster\Controllers", $controller_dir = ""){
		// $remote_group_key : primary, secondary, tertiery
		$prefix = config('keymaster.remote_auth.'.$remote_group_key.'.prefix');
		$namespace = $controller_dir == "" ? $namespace : $namespace.'\\'.trim($controller_dir, '\\');
		$this->remote_group_key = $remote_group_key; 
		Route::group([
				'middleware' => [ 'web' ], 
				'namespace' => $namespace ,
				'prefix' => $prefix,
				'group_key' => $remote_group_key
			], function () {
				Route::group(['middleware' => [ 'remote-auth:out' ]], function () {
					$key = $this->remote_group_key;
					$is_public = config('keymaster.remote_auth.'.$key.'.public');
					Route::get('/applink', 'RemoteAuth\RequestApplinkController@index')->name($key.'.app.applink.index');
					Route::post('/applink', 'RemoteAuth\RequestApplinkController@requestApplink')->name($key.'.app.applink');

					Route::get('/', 'RemoteAuth\AuthController@index')->name($key.'.user.landing');

					if($is_public){
						// Public Register
						Route::get('/register/{email?}', 'RemoteAuth\AuthController@registerIndex')->name($key.'.user.register.index');
						Route::post('/register/verify', 'RemoteAuth\AuthController@registerVerifyPublic')->name($key.'.user.register.verify');
					}else{
						// Private Register
						Route::get('/register/{email?}/{token?}', 'RemoteAuth\AuthController@registerPrivateIndex')->name($key.'.user.register.index');
					}
					
					Route::post('/register/submit', 'RemoteAuth\SendRegisterController@register')->name($key.'.user.register');
					Route::post('/activate/submit', 'RemoteAuth\SendActivateController@activate')->name($key.'.user.activate');
					Route::get('/forgot', 'RemoteAuth\AuthController@forgotIndex')->name($key.'.user.forgot.index');
					Route::get('/reset/{token}', 'RemoteAuth\AuthController@resetIndex')->name($key.'.user.reset.index');
					Route::post('/login/submit', 'RemoteAuth\SendLoginController@login')->name($key.'.user.login');
					Route::post('/requestReset/submit', 'RemoteAuth\SendResetPasswordController@requestReset')->name($key.'.user.request.reset');
					Route::post('/reset/submit', 'RemoteAuth\SendResetPasswordController@reset')->name($key.'.user.password.reset');
				});

				Route::group(['middleware' => [ 'remote-auth:in'  ] ], function () {
					$key = $this->remote_group_key;
					Route::post('/logout/submit', 'RemoteAuth\AuthController@logout')->name($key.'.user.logout');
					Route::get('/'.$key.'/dashboard', 'RemoteAuth\AuthController@dashboard')->name($key.'.user.dashboard');
					Route::get('/my/account', 'RemoteAuth\AuthController@account')->name($key.'.user.my.account');
					Route::get('/my/account/password', 'RemoteAuth\AuthController@accountPassword')->name($key.'.user.my.account.password');
					Route::post('/password/change', 'RemoteAuth\SendChangePasswordController@changePassword')->name($key.'.user.password.change');
				});
			}
		);
	}

	public function makeBreadRoutes( $remote_group_key , $item, $controller_file, $namespace = "App\Http\Controllers", $controller_dir = ""){
		// $remote_group_key : primary, secondary, tertiery
		$prefix = config('keymaster.remote_auth.'.$remote_group_key.'.prefix');
		$namespace = $controller_dir == "" ? $namespace : $namespace.'\\'.trim($controller_dir, '\\');
		$this->remote_group_key = $remote_group_key; 
		$this->item = $item;
		$this->controller = $controller_file; 
		Route::group([
				'middleware' => [ 'web' ], 
				'namespace' => $namespace ,
				'prefix' => $prefix,
				'group_key' => $remote_group_key,
			], function () {
				Route::group(['middleware' => [ 'remote-auth:in'  ] ], function () {
					$key = $this->remote_group_key;
					$item = $this->item;
					$controller = $this->controller;
					Route::get('/'.$item.'/index', $controller.'@index')->name( $key.'.bread.'.$item.'.index');
					Route::get('/'.$item.'/browse', $controller.'@browse')->name( $key.'.bread.'.$item.'.browse');
					Route::get('/'.$item.'/read/{userID?}', $controller.'@read')->name( $key.'.bread.'.$item.'.read');
					Route::get('/'.$item.'/add/index', $controller.'@addIndex')->name( $key.'.bread.'.$item.'.add.index');
					Route::get('/'.$item.'/edit/index', $controller.'@editIndex')->name( $key.'.bread.'.$item.'.edit.index');
					Route::post('/'.$item.'/add', $controller.'@add')->name( $key.'.bread.'.$item.'.add');
					Route::post('/'.$item.'/edit', $controller.'@edit')->name( $key.'.bread.'.$item.'.edit');
					Route::post('/'.$item.'/delete', $controller.'@delete')->name( $key.'.bread.'.$item.'.delete');
				});
			}
		);
	}

	public function makePresetRegistryRoute( $remote_group_key, $item , $registry_dir , $namespace = "App\Http\Controllers", $controller_dir = ""){
		$this->makeBreadRoutes( $remote_group_key , 'registry_'.$item , $registry_dir.'\UserBreadController' , $namespace , $controller_dir );
        $this->makeBreadRoutes( $remote_group_key , 'registry_'.$item.'_token' , $registry_dir.'\RegisterTokenBreadController', $namespace , $controller_dir);

        $prefix = config('keymaster.remote_auth.'.$remote_group_key.'.prefix');
		$namespace = $controller_dir == "" ? $namespace : $namespace.'\\'.trim($controller_dir, '\\');
		$this->remote_group_key = $remote_group_key; 
		$this->item = $item;
		$this->registry_dir = $registry_dir;  
        Route::group([
				'middleware' => [ 'web' ], 
				'namespace' => $namespace ,
				'prefix' => $prefix,
				'group_key' => $remote_group_key,
			], function () {
				Route::group(['middleware' => [ 'remote-auth:in'  ] ], function () {
					$key = $this->remote_group_key;
					$item = $this->item;
					Route::get( '/registry_'.$item.'_token/send/index', $this->registry_dir.'\SendRegisterTokenController@index')->name($key.'.preset.registry_'.$item.'_token.send.index');
					Route::post( '/registry_'.$item.'_token/send', $this->registry_dir.'\SendRegisterTokenController@send')->name($key.'.preset.registry_'.$item.'_token.send');
				});
			}
		);
	}

	public function makeBreadRoutesV2( $remote_group_key , $items ,$item, $controller_file, $unique="" ,$namespace = "App\Http\Controllers", $controller_dir = ""){
		// $remote_group_key : primary, secondary, tertiery
		$prefix = config('keymaster.remote_auth.'.$remote_group_key.'.prefix');
		$namespace = $controller_dir == "" ? $namespace : $namespace.'\\'.trim($controller_dir, '\\');
		$this->remote_group_key = $remote_group_key; 
		$this->item = $item;
		$this->items = $items;
		$this->unique = $unique;
		$this->controller = $controller_file; 
		Route::group([
				'middleware' => [ 'web' ], 
				'namespace' => $namespace ,
				'prefix' => $prefix,
				'group_key' => $remote_group_key,
			], function () {
				Route::group(['middleware' => [ 'remote-auth:in'  ] ], function () {
					$key = $this->remote_group_key;
					$item = $this->item;
					$items = $this->items;
					$name_item = $item;
					$unique = $this->unique;

					// Add Unique Route Key into address and route name
					if( $unique != "" ){
						$name_item = $unique.".".$item;
						$item = $unique."/".$item;
						$items = $unique."/".$items;	
					}

					$controller = $this->controller;
					Route::get('/'.$items, $controller.'@browse')->name( $key.'.bread.'.$item.'.browse');
					Route::get('/'.$item.'/add', $controller.'@add')->name( $key.'.bread.'.$item.'.add');			
					Route::get('/'.$item.'/{id}/edit', $controller.'@edit')->name( $key.'.bread.'.$item.'.edit');
					Route::get('/'.$item.'/{id}/delete', $controller.'@delete')->name( $key.'.bread.'.$item.'.delete');
					Route::get('/'.$item.'/{id}', $controller.'@read')->name( $key.'.bread.'.$item.'.read');	
					Route::post('/'.$item.'/save', $controller.'@save')->name( $key.'.bread.'.$item.'.save');
					Route::post('/'.$item.'/{id}/update', $controller.'@update')->name( $key.'.bread.'.$item.'.update');
					Route::post('/'.$item.'/{id}/destroy', $controller.'@destroy')->name( $key.'.bread.'.$item.'.destroy');
				});
			}
		);
	}

	public function makePresetRegistryRouteV2( $remote_group_key , $items ,$item, $controller_file, $unique="" ,$namespace = "App\Http\Controllers", $controller_dir = ""){

		$this->makeBreadRoutesV2( $remote_group_key , $items ,$item, $controller_file, $unique="" ,$namespace = "App\Http\Controllers", $controller_dir = "");

        $prefix = config('keymaster.remote_auth.'.$remote_group_key.'.prefix');
		$namespace = $controller_dir == "" ? $namespace : $namespace.'\\'.trim($controller_dir, '\\');
		$this->remote_group_key = $remote_group_key; 
		$this->item = $item;
		$this->unique = $unique;
		$this->controller = $controller_file; 
        Route::group([
				'middleware' => [ 'web' ], 
				'namespace' => $namespace ,
				'prefix' => $prefix,
				'group_key' => $remote_group_key,
			], function () {
				Route::group(['middleware' => [ 'remote-auth:in'  ] ], function () {
					$key = $this->remote_group_key;
					$item = $this->item;
					$name_item = $item;
					$unique = $this->unique;

					// Add Unique Route Key into address and route name
					if( $unique != "" ){
						$name_item = $unique.".".$item;
						$item = $unique."/".$item;
						$items = $unique."/".$items;	
					}

					$controller = $this->controller;
					Route::post('/'.$item.'/resend', $controller.'@resend')->name( $key.'.bread.'.$item.'.resend');
				});
			}
		);
	}

	public function makeOptions( $data , $option_value_key , $option_name_key ){
		$options = [];
		foreach( $data as $d ){
			$options[ $d->$option_value_key ] = $d->$option_name_key;
		}

		return (object)$options;
	}
}
