<?php

namespace Keymaster;

use Illuminate\Support\ServiceProvider;
use Keymaster\console\InitKeymasterCommand;
use Keymaster\console\CreateUserGroupCommand;
use Keymaster\console\InitManageSlaveCommand;

class WCKeymasterServiceProvider extends ServiceProvider
{
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {      
        // load Gatekeeper Views
        $this->loadViewsFrom(__DIR__.'/views', 'KMView');
        
        // load Gatekeeper Migrations
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        // load Gatekeeper Routes
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->publishes([
        __DIR__.'/init/config' => base_path('/config'),       
        ],"keymaster-config");

        $this->publishes([
        __DIR__.'/create_usergroup/views' => resource_path('/views'),       
        ],"keymaster-auth");

        $this->publishes([
        __DIR__.'/addon/ManageSlave/init/config' => base_path('/config'),       
        ],"manage-slave-config");

        $this->bootAddonBread();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {   
        // bind this
        $this->app->bind('keymaster-object',function() {
            return new Keymaster();
        });

        $this->app['router']->aliasMiddleware('remote-auth', "Keymaster\Middleware\RemoteAuth");

        $this->app->singleton( 'command.keymaster.init' , function($app)
        {
            return new InitKeymasterCommand;
        });
        $this->app->singleton( 'command.keymaster.new-auth' , function($app)
        {
            return new CreateUserGroupCommand($app['files']);
        });
        $this->app->singleton( 'command.manageslave.init' , function($app)
        {
            return new InitManageSlaveCommand;
        });

        $this->commands( 'command.keymaster.init' );
        $this->commands( 'command.keymaster.new-auth' );
        $this->commands( 'command.manageslave.init' );
    }

    protected function bootAddonBread(){
        $this->loadViewsFrom(__DIR__.'/addon/ManageBread/views', 'KMBreadView');
    }

}

