<?php
/*
    |--------------------------------------------------------------------------
    | Remote Auth Settings
    |--------------------------------------------------------------------------
    |'group_key' => [
	|		'user_dir' => 'user',													// view folder directory		
	|		'prefix' => '',															// prefix in route
	|		'public' => env('PRIMARY_REMOTE_PUBLIC', false ),						// register require register token
	|		'role_key' => env('PRIMARY_REMOTE_ROLE_KEY', '' ), 						// role key to link to gatekeeper 
	|		'api_connect' => env('PRIMARY_REMOTE_API_CONNECT', 'passport-api' ),	// manual/ passport-api
	|		'api_name' => env('PRIMARY_REMOTE_API_NAME', '' ), 						// passport-api name
	|		'api_url' => env('PRIMARY_REMOTE_API_URL', '' ),						// manual - url
	|		'api_token' => env('PRIMARY_REMOTE_API_TOKEN', '' ),					// manual token
			
			// bottom options only required to add for customize
			'authenticated_redirect' => 'primary.user.dashboard',					// redirect route if logged in
			'unauthenticated_redirect' => 'primary.user.landing',					// redirect route if not logged in
			'login_after_register' => true,											// should login after register
	|	], 
    | 	



    */

return [
		'remote_auth' => [
			'primary' => [
				'user_dir' => 'user',													
				'prefix' => '',
				'public' => env('PRIMARY_REMOTE_PUBLIC', false ),
				'role_key' => env('PRIMARY_REMOTE_ROLE_KEY', '' ), 
				'api_connect' => env('PRIMARY_REMOTE_API_CONNECT', 'passport-api' ),	// manual/ passport-api
				'api_name' => env('PRIMARY_REMOTE_API_NAME', '' ), 						// passport-api name
				'api_url' => env('PRIMARY_REMOTE_API_URL', '' ),						// manual - url
				'api_token' => env('PRIMARY_REMOTE_API_TOKEN', '' ),					// manual token
			],
			'secondary' => [
				'user_dir' => 'user2',
				'prefix' => 'user2',
				'public' => env('SECONDARY_REMOTE_PUBLIC', false ),
				'role_key' => env('SECONDARY_REMOTE_ROLE_KEY', 'wooyoo-manage' ), 
				'api_connect' => env('SECONDARY_REMOTE_API_CONNECT', 'passport-api' ),	// manual/ passport-api
				'api_name' => env('SECONDARY_REMOTE_API_NAME', 'WooYoo Manage' ), 		// passport-api name
				'api_url' => env('SECONDARY_REMOTE_API_URL', '' ),						// manual - url
				'api_token' => env('SECONDARY_REMOTE_API_TOKEN', '' ),					// manual token
			],
		],
];
