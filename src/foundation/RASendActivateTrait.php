<?php

namespace Keymaster\foundation;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Keymaster\Models\RemoteAuth;
use Keymaster\foundation\APIRemoteAuthTrait;

trait RASendActivateTrait
{
  use APIRemoteAuthTrait;

  public function activate(Request $request){  
    $params = $request->all();

    if( config('keymaster.remote_auth.'.$this->group_key.'.public') ){
      $result = $this->sendAPI( "activate", $params );
    }else{
      $result = $this->sendAPI( "activatePrivate", $params );
    }


    if($result->Resp->Status == 'F' ) {
      return redirect()->back()
        ->withInput($request->only('email', 'name'))
        ->withErrors( $result->Resp->FailedReason );
    }else{ 

      // Success create new account
      $remote_auth = $this->getAuthModel();
      $remote_auth = $remote_auth->where('email', $request->email )
                                  ->where('role_key', $this->RoleKey())
                                  ->first();

      if(!$remote_auth){
        $remote_auth = $this->getAuthModel();
        $remote_auth->email = $result->Result->email;
        $remote_auth->role_key = $result->Result->role_key;
      }                          
      $remote_auth->remote_account_id = $result->Result->remote_account_id;
      $remote_auth->remote_account_token = $result->Result->remote_account_token;
      $remote_auth->remote_session_token = $result->Result->remote_session_token;
      $remote_auth->save();


      if( $this->login_after_register() ){
        $this->cradleHint()->authMessage($request, "login", $remote_auth->email);
        Auth::guard('web_remote')->loginUsingId($remote_auth->id);
        return redirect()->route( $this->auth_in_redirect() );
      }

      return redirect()->back()
        ->withMessage("Account ". $result->Req->email." is successfully Activated." );
    }
  }
}
