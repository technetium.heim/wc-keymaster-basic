<?php

namespace Keymaster\foundation;

use Illuminate\Http\Request;
use Auth;
use Keymaster\foundation\APIRemoteAuthTrait;


trait RASendResetPasswordTrait
{
  use APIRemoteAuthTrait;
 
  public function requestReset(Request $request)
  {
    
    $params = $request->all();
    
    $params['reset_password_route'] = route($this->group_key.'.user.reset.index',['token' => '']);

    $result = $this->sendAPI( "ResetPasswordEmail", $params );

    if($result->Resp->Status == 'F' ) {
      return redirect()->back()
        ->withInput($request->only('email'))
        ->withErrors( $result->Resp->FailedReason );
    }else{ 
      // Response
      return redirect()->back()
        ->withMessage("Reset Password Email is sent to ". $request->email );
    }
  }

  public function reset(Request $request)
  {
    $params = $request->all();

    $result = $this->sendAPI( "ResetPassword", $params );

    if($result->Resp->Status == 'F' ) {
      return redirect()->back()
        ->withInput($request->only('email'))
        ->withErrors( $result->Resp->FailedReason );
    }else{ 

      $remote_auth = $this->getAuthModel();
      $remote_auth = $remote_auth->where('email', $request->email)
                              ->where('role_key', $this->loginRoleKey())
                              ->first();

      if( $this->login_after_register() ){
        Auth::guard('web_remote')->loginUsingId($remote_auth->id);
        $this->cradleHint()->authMessage($request, "login", $remote_auth->email);
        return redirect()->route( $this->auth_in_redirect() );
      }

      // Response
      return redirect()->back()
        ->withMessage("Password is Reset." );
    }
  }
}
