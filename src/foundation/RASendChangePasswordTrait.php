<?php

namespace Keymaster\foundation;

use Illuminate\Http\Request;
use Auth;
use Keymaster\foundation\APIRemoteAuthTrait;


trait RASendChangePasswordTrait
{
  use APIRemoteAuthTrait;
 
  public function changePassword(Request $request)
  {
    $params = $request->all();
    $params['email'] = Auth::guard('web_remote')->user()->email;
    $result = $this->sendAPI( "PasswordChange", $params );

    if($result->Resp->Status == 'F' ) {
      return redirect()->back()
        ->withErrors( $result->Resp->FailedReason );
    }else{ 
      // Response
      return redirect()->back()
        ->withMessage( "Password Updated." );
    }
  }

}
