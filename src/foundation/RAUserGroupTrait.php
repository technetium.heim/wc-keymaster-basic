<?php

namespace Keymaster\foundation;

use Route;
use Auth;
/*
  This Controller is only for simple auth page (view).
  The only method is Logout. 
*/
trait RAUserGroupTrait
{
	protected $group_key;

  public function __construct()
  {
    // to get group key for current route
    if( array_key_exists( 'group_key' , Route::getCurrentRoute()->getAction() ) ){
    	$this->group_key = Route::getCurrentRoute()->getAction()['group_key'];
	   }

    $this->page['group_key'] =  $this->group_key;
    $this->user_dir = $this->group_key != null ? config('keymaster.remote_auth.'.$this->group_key.'.user_dir') : 'user';
    $this->page['user_dir'] = $this->user_dir;
  }

  protected function getAuthModel(){
    $model = Auth::guard("web_remote")->getProvider()->getModel();
    return $model::getModel();
  }

  protected function loginRoleKey(){
    return config( $this->getRemoteAuth().'.'.$this->group_key.'.role_key');
  }

  protected function auth_out_redirect(){
    if ( config( $this->getRemoteAuth().'.'.$this->group_key.'.unauthenticated_redirect') == null)
      return $this->default_val('unauthenticated_redirect');
    else
      return config( $this->getRemoteAuth().'.'.$this->group_key.'.unauthenticated_redirect');
  }

  protected function auth_in_redirect(){
    if ( config( $this->getRemoteAuth().'.'.$this->group_key.'.authenticated_redirect') == null)
      return $this->default_val('authenticated_redirect');
    else
      return config( $this->getRemoteAuth().'.'.$this->group_key.'.authenticated_redirect');
  }

  protected function login_after_register(){
    if ( config( $this->getRemoteAuth().'.'.$this->group_key.'.login_after_register') == null)
      return $this->default_val('login_after_register');
    else
      return config( $this->getRemoteAuth().'.'.$this->group_key.'.login_after_register');
  }

  protected function getRemoteAuth(){
    return 'keymaster.remote_auth';
  }

  protected function default_val($key){

    switch ($key) {
      case 'authenticated_redirect':
        return $this->group_key.'.user.dashboard';
        break;
      case 'unauthenticated_redirect':
        return $this->group_key.'.user.landing';
        break;
      case 'login_after_register':
        return true;
        break;
      default:
        return null;
        break;
    }
  } 
}
