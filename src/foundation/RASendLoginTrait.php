<?php

namespace Keymaster\foundation;

use Illuminate\Http\Request;
use Auth;
use Validator;

use Keymaster\foundation\APIRemoteAuthTrait;

trait RASendLoginTrait
{
  use APIRemoteAuthTrait;
  
  ////////////////// Settings
  public function login(Request $request){  

    $params = $request->all();

    $result = $this->sendAPI( "login", $params );
    
    if($result->Resp->Status == 'F' ) {
      return redirect()->back()
        ->withInput($request->only('email', 'name'))
        ->withErrors( $result->Resp->FailedReason );
    }else{ 
      
      $remote_auth = $this->getAuthModel();
      $remote_auth = $remote_auth->where('email', $request->email)
                                ->where('role_key', $this->loginRoleKey())
                                ->first();
    

      if( !$remote_auth ){
        $remote_auth = $this->getAuthModel();
        $remote_auth->email = $result->Result->email;
        $remote_auth->role_key = $this->loginRoleKey();
        $remote_auth->remote_account_id = $result->Result->remote_account_id;
        $remote_auth->remote_account_token = $result->Result->remote_account_token;
      }

      $remote_auth->remote_session_token = $result->Result->remote_session_token;
      $remote_auth->save();

      $request->session()->flush();
      
      Auth::guard('web_remote')->loginUsingId($remote_auth->id);
      $this->cradleHint()->authMessage($request, "login", $remote_auth->email);
      return redirect()->route( $this->auth_in_redirect() );
    }
  }

  protected function validateInputs( $request ){
      // Validation 
      $rules = [
          'email' => 'required',
          'password' => 'required',
      ];

      $message = [
          'email.required' => 'Email is missing.',
          'password.required' => 'Password is missing.',
      ];

      $validator = Validator::make( $request, $rules, $message );

      return $validator;
  }

  
}
