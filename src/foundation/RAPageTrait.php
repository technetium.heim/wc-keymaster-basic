<?php

namespace Keymaster\foundation;

use Illuminate\Http\Request;
use Auth;
use Session;
// use Keymaster\foundation\RAUserGroupTrait;
use Keymaster\foundation\APIRemoteAuthTrait;
/*
  This Controller is only for simple auth page (view).
  The only method is Logout. 
*/
trait RAPageTrait
{
  // use RAUserGroupTrait;
  use APIRemoteAuthTrait;

  protected $page;
  protected $redirectTo = '/';

  public function index()
  {
    // dd( Auth::guard("web_remote")->getProvider()->createModel() );
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/auth/landing',$this->page);
  }

  public function dashboard(){
    $this->page['title'] = 'Dashboard';
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/dashboard/main', $this->page);
  }

  public function account(){
    $this->page['title'] = 'My Account';
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/account/read', $this->page);
  }

  public function accountPassword(){
    $this->page['title'] = 'Change Password';
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/account/password/edit', $this->page);
  }

  public function registerIndex( $email="" ){
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));

    if( $email == "" ){
      $this->page['load_page'] = 'verify';
      return view( $this->user_dir.'/pages/auth/register', $this->page);
    }

    $this->page['email'] = $email;
    
    $params=[
      'email' =>  $email
    ];
    $result = $this->sendAPI( "registerVerify", $params );

    if($result->Resp->Status == 'F' ) {
      $this->page['load_page'] = 'verify';
      return view( $this->user_dir.'/pages/auth/register', $this->page);
    }
    
    $this->page['load_page'] = $result->Result->action;

    return view( $this->user_dir.'/pages/auth/register', $this->page);
  }

  public function registerVerifyPublic( Request $request ){
    return redirect()->route( $this->group_key.'.user.register.index', [ 'email' => $request->email ] );
  }

  public function registerPrivateIndex( $email= "", $token ="" ){
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));

    if($email == "" || $token == "" ){
      $this->page['load_page'] = 'broken';
      return view( $this->user_dir.'/pages/auth/register', $this->page);
    }

    $this->page['email'] = $email;
    $this->page['token'] = $token;

    $params=[
      'email'           =>  $email,
      'register_token'  =>  $token
    ];

    $result = $this->sendAPI( "registerPrivateVerify", $params );

    if($result->Resp->Status == 'F' ) {
      $this->page['load_page'] = 'broken';
      return view( $this->user_dir.'/pages/auth/register', $this->page);
    }

    $this->page['load_page'] = $result->Result->action;
    
    return view( $this->user_dir.'/pages/auth/register', $this->page);
  }

  public function activateIndex($email= "", $token =""){

    $this->page['email'] = $email;
    $this->page['token'] = $token;

    if(!$email || !$token) $this->page['autofill'] = false;
    else $this->page['autofill'] = true; 

    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/auth/activate', $this->page);
  }

  public function forgotIndex(){
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/auth/forgot', $this->page);
  }

  public function resetIndex($token){
    $this->page['token'] = ucwords($token);
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view( $this->user_dir.'/pages/auth/reset', $this->page);
  }

  // logout
  public function logout(Request $request){  
    // start login session 
    Auth::guard('web_remote')->logout();
    Session::flush();
    $this->cradleHint()->authMessage($request, "logout");
    return redirect()->route( $this->auth_out_redirect() );
  }
}