<?php

namespace Keymaster\foundation;

use GuzzleHttp\Client;
use WCPassportApi\models\PassportApi;
use WCPassportApi\supports\PassportToken;
use Keymaster\foundation\RAUserGroupTrait;
use Log;

trait APIRemoteAuthTrait
{
    use RAUserGroupTrait;
    protected function userGroup(){
        return $this->group_key; //default
    }

    // create Keymaster Routes

    // To setup default data to send API
    protected function default_data(){
        return array(
            "connect_app_name" => config('app.name'),
            "connect_role_key" => $this->RoleKey(),
            "connect_remote_url" => url('/'),
        );
    }

    protected function uri_base(){      
        // **to add: remove '/' at the end of url for standard
        if($this->AuthConfig()['api_connect'] == 'passport-api'){
            return $this->passport()->uri."api/";
        }
        return $this->AuthConfig()['api_url'];
    }

    protected function bearerToken(){
        if($this->AuthConfig()['api_connect'] == 'passport-api'){
            return $this->passport()->token;
        }
        return $this->AuthConfig()['api_token'];
    }

	protected function sendEmptyAPI( $target, $param = [] , $method="POST" )
	{
		return $this->sendAPI( $target, $param, $method, false);
	}

	protected function sendAPI( $target, $param = [] , $method="POST", $default = true )
	{
		//set default
            $status = 'pending';
            $origin_message = '<b>Internal Server Error</b><br>Please contact admin.';
            $response = (object) array('status'=>$status, 'message'=>$origin_message);

            $this->page['result'] = '';
            $this->page['json'] = '';

        
        //set request
            $r['uri'] = $this->uri_base().$target;
            $r['headers'] = array(
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$this->bearerToken(),
                'Content-Type' => 'application/json',
            );

            if( $default == true ){
                //set params
	            $r['params'] = $this->default_data();
        	}else {
        		$r['params'] = array();
        	}
        	
            if( !empty($param) ){
            	$r['params'] = array_merge( $r['params'] , $param );
            }

        //retrieve info
            $http = new Client;

            $response = $http->request( $method ,$r['uri'], [
                'headers' => $r['headers'],
                'query' => "",
                'json' =>  $r['params'],
                'verify' => false
            ]);

            $sessionidx = $response->getHeader("Set-Cookie");

            if( $sessionidx ){
                $sessionidx = str_replace('SESSIONIDX=', '',$sessionidx[0]);
                $sessionidx = str_replace('; path=/; HttpOnly', '',$sessionidx);
                $this->idx = $sessionidx;
            }

            $data = json_decode($response->getBody());


            return $data;
	}

    protected function AuthConfig(){
        // return config( 'remoteAuth.'.config('app.env').'.'.$this->userGroup() );
        return config( 'keymaster.remote_auth.'.$this->userGroup() );
    }

    protected function passport(){
        $passport_api_name = $this->AuthConfig()['api_name'];
        // $passport = PassportApi::where('name', $passport_api_name )->first();
        return $passport = PassportToken::getTokenLocal($passport_api_name);
        if( $passport == null ) Log::debug('Cannot Match Passport API in database'); 
        return $passport;
    }

    protected function RoleKey(){
        return $this->AuthConfig()['role_key'];
    }
}