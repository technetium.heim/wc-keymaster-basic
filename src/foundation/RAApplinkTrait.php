<?php

namespace Keymaster\foundation;

use Illuminate\Http\Request;

use Keymaster\foundation\APIRemoteAuthTrait;

trait RAApplinkTrait
{
	use APIRemoteAuthTrait;

	public function index(){
	    $this->page['title'] = ucwords(config('app.name'));
	    $this->page['meta']['description'] = ucwords(config('app.name'));
	    return view( $this->user_dir.'/pages/auth/applink', $this->page);
	}

	public function requestApplink(Request $request)
	{
	  	// **to add: the data should saved in config/ env
	  	// $param = array(
	  	// 	'app_name' => config('app.name'),
	  	// 	'base_url' => url('/'),
	  	// 	'role_key' => 'wooyoo-manage',
	  	// 	'public' => false,
	  	// );
	    $param = array(
	     'app_name' => $request->app_name,
	     'base_url' => $request->base_url,
	     'role_key' => $request->role_key,
	     'public' => ($request->role_key == 'on' )? true:false,
	    );
	
	    $result = $this->sendEmptyAPI( "applink", $param );
    
	    if($result->Resp->Status == 'F' ) {
	      return redirect()->back()
	        ->withInput($request->only('role_key'))
	        ->withErrors( $result->Resp->FailedReason );
	    }else{ 
	      // Response
	      return redirect()->back()
	        ->withMessage("Application Link request is sent." );
	    }

	    // Temporary
	    return 
	      "<pre>".
	        json_encode($result->Resp, JSON_PRETTY_PRINT)
	      ."</pre>
	      <a href='".route( 'user.primary.landing' )."'>Back to Landing Page</a>";
	}

}
