<?php 

namespace Keymaster\facades;

use Illuminate\Support\Facades\Facade;


class Keymaster extends Facade 
{
	protected static function getFacadeAccessor()
	{
		return "keymaster-object";
	}

}