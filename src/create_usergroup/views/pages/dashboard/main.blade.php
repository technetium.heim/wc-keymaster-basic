@extends('WCView::general.layouts.admin.desktop.primary')

@section('menu')
	@include($user_dir.'.menus.primary')
@endsection

@section('subheader')
	<div class="height-50 row border-bottom">
		<div class="col-xs-6">
			<div class="textbox-50 text-18 text-black text-left">{{$title}}</div>
		</div>
		<div class="col-xs-6">
			<div class="pull-right">
				<i class="icon-50 material-icons text-unavailable">search</i>
			</div>
		</div>
	</div>
@endsection

@section('content')
	@php
		// blue = #27a9e3 / #2194ca
		// green = #24b87a / #17a668
		// purple = #852c9a / #741d88
		// orange = #ffb849 / #da9627
		// red = #dd4b39 / #c64333
		// lightblue = #5c9af0 / #4886db
		// yellow = #fecf49 / #f6bc35
		// gray = #ccd0d9 / #a9b1bc 

		$items = array
	    (
	    	(object) array(
	            "text" => "Profile",
	            "icon" => "fa-user",
	            "color" => "#852c9a",
	            "darken_color" => "#741d88",
	            "uri" =>  ""
	        ), 
	        (object) array(
	            "text" => "Service",
	            "icon" => "fa-wrench",
	            "color" => "#24b87a",
	            "darken_color" => "#17a668",
	            "uri" => ""
	        ), 
	        (object) array(
	            "text" => "Order",
	            "icon" => "fa-cart-arrow-down",
	            "color" => "#27a9e3",
	            "darken_color" => "#2194ca",
	            "uri" => ""
	        ),
	        (object) array(
	            "text" => "Example",
	            "icon" => "fa-cart-arrow-down",
	            "color" => "#ffb849",
	            "darken_color" => "#da9627",
	            "uri" => ""
	        ),
	    );
	@endphp

	<div class="row">
		@foreach( $items as $item )
			<div class="col-xs-3 padding-right">
				<div class="" style="background-color: {{ $item->color }}">
					<div class="row text-2x text-white padding">
						<div class="col-xs-3 text-center">
							<i class="fa fa-lg {{ $item->icon }}"></i>
						</div>
						<div class="col-xs-9 text-right">
							{{ $item->text }}
						</div>
					</div>
					<div class="row padding-xs"  style="background-color: {{ $item->darken_color }}">
						<a class="text-white" href="{{ $item->uri }}">
							<div>
								View Detail
								<div class="pull-right text-white">
									<i class="fa fa-arrow-circle-o-right fa-lg"></i>
								</div>
							</div>
						</a>
					</div>
				</div>							
			</div>
		@endforeach
	</div>
@endsection