@extends('WCView::general.layouts.admin.desktop.primary')

@section('menu')
	@include($user_dir.'.menus.primary')
@endsection

@section('subheader')
	<div class="height-50 row border-bottom">
		<div class="col-xs-12">
			<div class="textbox-50 text-18 text-black text-left">{{$title}}</div>
		</div>
	</div>
@endsection

@section('content')
	@include('KMView::account.content_account')
@endsection