<?php

namespace Keymaster\Models;

use App\Models\AppCradleModel;
use Cradle\basic\foundation\auth\CradleUser as Authenticatable;

class RemoteAuth extends Authenticatable
{
    
    protected $table = 'remote_auth';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email','remote_account_id','remote_account_token','remote_session_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
