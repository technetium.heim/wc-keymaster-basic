<?php

namespace Keymaster\addon\ManageMaster\Models;

use App\Models\AppCradleModel;

class ManageCheckToken extends AppCradleModel
{
    protected $table = 'manage_check_tokens';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['remote_account_id','remote_account_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
  
}
