<?php

namespace Keymaster\addon\ManageMaster;

use Illuminate\Support\ServiceProvider;
use Keymaster\console\InitKeymasterCommand;
use Keymaster\console\CreateUserGroupCommand;

class ManageMasterServiceProvider extends ServiceProvider
{
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {      
        // load Gatekeeper Migrations
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {   
        $this->app['router']->aliasMiddleware('app_manage', "Keymaster\addon\ManageMaster\Middleware\AppToManage");
    }   

}

