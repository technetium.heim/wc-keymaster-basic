<?php

namespace Keymaster\addon\ManageMaster\Middleware;

use Closure;
use Keymaster\addon\ManageMaster\Models\ManageCheckToken;
use Keymaster\foundation\APIRemoteAuthTrait;
use Keymaster\addon\ManageMaster\Support\APIResponseTrait;
use Validator;
use Carbon\Carbon;

class AppToManage
{
    use APIRemoteAuthTrait;
    use APIResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $request_param = $request->all();

        $validator = $this->validateInputs( $request_param );

        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }
 
        $manage_check = ManageCheckToken::where('remote_account_id', $request->remote_account_id)->first();
        if( !$manage_check ){
            $result = $this->sendAPI( "verifyUserFromManage", $request->all() );
         
            if($result->Resp->Status == 'F' ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $result->Resp->FailedReason ) );
                return response()->json( $response );
            }

            // create new manage_check
            $manage_check = new ManageCheckToken;
            $manage_check->remote_account_id = $result->Result->remote_account_id;
            $manage_check->remote_account_token = $result->Result->remote_account_token;
            $manage_check->save();
        }

        // If Token Mismatch
        if ($manage_check->remote_account_token != $request->remote_account_token ) {
            $result = $this->sendAPI( "verifyUserFromManage", $request->all() );
         
            if($result->Resp->Status == 'F' ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $result->Resp->FailedReason ) );
                return response()->json( $response );
            }

            // create new manage_check
            $manage_check->remote_account_id = $result->Result->remote_account_id;
            $manage_check->remote_account_token = $result->Result->remote_account_token;
            $manage_check->save();
        }

        // If Expired
        $expired = $manage_check->updated_at->addDay(); // EXPIRED IF UPDATED_AT COL IS 1 DAY OLD
        $now = Carbon::now();
        if( $now->gt($expired) ){
            $result = $this->sendAPI( "verifyUserFromManage", $request->all() );
         
            if($result->Resp->Status == 'F' ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $result->Resp->FailedReason ) );
                return response()->json( $response );
            }

            $manage_check->touch();
        }

        return $next($request);
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'remote_account_id' => 'required',
            'remote_account_token' => 'required',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'remote_account_id.required' => 'Remote Account ID is missing.',
            'remote_account_token.required' => 'Remote Account Token is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }
}