<?php

namespace App\Http\Controllers\User\RegistryCompany;

use App\Http\Controllers\CradleController;
use Keymaster\addon\ManageBread\foundation\KeymasterBreadTraitV2;
use App\Http\Controllers\APIResponseTrait;
use Validator;

use Illuminate\Http\Request;

class UserBreadController extends CradleController
{	
	use KeymasterBreadTraitV2;
	use _RegistrySettingTrait;
	use APIResponseTrait;

	protected $page;
	protected $redirectTo = '/';
	
	/// Settings (Bread Browse & Read)
	protected function setupBreadBase(){
		// Basic
		$this->bread['table_item_singular'] = $this->registryItem()->singular;
		$this->bread['table_item_plural'] = $this->registryItem()->plural;
		$user_group = $this->registryAccessGroupKey();

		// Browse Page Route Back
		$this->bread['browse_route_back'] = '';

		// Browse Table Column
		$this->bread['browse_cols'] = ['email'];
		$this->bread['browse_cols_sm'] = []; 
		$this->bread['browse_cols_xs'] = [];
		
		//Read Table Column
		$this->bread['read_cols'] = ['email'];

		// Table Item
		

		// Enable/Disable Action ( Control Button Show/ Hide )
		$this->bread['action_read'] = true;  		// @Browse Page 
		$this->bread['action_edit'] = true;			// @Read Page
		$this->bread['action_add'] = true;			// @Browse Page
		$this->bread['action_delete'] = false; 		// @Read Page

		// Action Route ( $this->KMBreadRoute( $key, $user_group , $item ); )
		$item  = $this->registryItem()->singular;
		$this->bread['route_browse'] = $this->KMBreadRoute( 'browse', $user_group , $item );  
		$this->bread['route_read'] = $this->KMBreadRoute( 'read', $user_group , $item ); 
		$this->bread['route_add'] = $this->KMBreadRoute( 'add', $user_group , $item ); 
		$this->bread['route_save'] = $this->KMBreadRoute( 'save', $user_group , $item ); 
		$this->bread['route_update'] = $this->KMBreadRoute( 'update', $user_group , $item ); 
		$this->bread['route_destroy'] = $this->KMBreadRoute( 'destroy', $user_group , $item ); 

		// Action Route (custom edit/delete page) -Button trigger Redirect 
		$this->bread['route_edit'] = '';		//GET Page
		$this->bread['route_delete'] = '';		//GET Page

		// ID Target for Route
		$this->bread['read_target_col_name'] = "id"; 				// Column Name of id data use for Read
		$this->bread['edit_target_col_name'] = "id"; 			// Column Name of id data use to Update
		$this->bread['delete_target_col_name'] = "id";			// Column Name of id data use to Delete

		// Custom Page Title 
		$this->bread['title_browse'] = 'default';
		$this->bread['title_read'] = 'default';
		$this->bread['title_add'] = 'Invite Target';

		// Button trigger (Action / Redirect )
		$this->bread['button_edit_type'] = 'default'; // (use 'redirect' to redirect to 'route_edit')
		$this->bread['button_delete_type'] = 'default'; // (use 'redirect' to redirect to 'route_delete')

		// Custom Button Words
		$this->bread['button_add'] = 'Invite';
		$this->bread['button_edit'] = 'default';
		$this->bread['button_delete'] = 'default';
		$this->bread['button_add_save'] = 'Send Invitation';
		$this->bread['button_update_save'] = 'default';
		$this->bread['button_update_cancel'] = 'default';

		// Addon Read Page Include View 
		$this->bread['page_read_include'] = [];
	}

	protected function setupViewPagePath(){
		$this->view['browse'] = $this->KMBreadView( 'browse' );		//Browse Page
		$this->view['read'] = $this->KMBreadView( 'read' );			//Read/ Edit Page
		$this->view['add'] = $this->KMBreadView( 'add' );			//Add Page

		$this->form['add'] = $this->KMBreadForm();					//Add Form
		$this->form['edit'] = $this->KMBreadForm();					//Edit Form
	}

	protected function setupBreadInputs( $action_type ){
		//Edit Form Column (default)
		//Edit Form Cols same as Read Table Column
		$this->bread['edit_cols_locked'] = ['email'];  // Columns disabled to Edit (Cannot Edit)
		$this->bread['edit_cols_required'] = []; 					// Columns required to Edit (Required to Submit + * )
		$this->bread['edit_cols_hidden'] = []; 						// Columns Hidden With Data	

		//Add Form Column (default)
		$this->bread['add_cols'] = ['email'];
		$this->bread['add_cols_locked'] = []; 						// Columns disabled to Edit (Cannot Edit)
		$this->bread['add_cols_required'] = ['email']; 				// Columns required to Edit (Required to Submit + * )
		$this->bread['add_cols_hidden'] = [];						// Columns Hidden With Data	
	}

	protected function setupCustomizeBreadEditInputs(){
		// Replace The default Text Input
		//Customize Edit Inputs ( Run After Required and Hidden list);
		// 	$this->createHiddenInput( $col, $value = null );
		// 	$this->createDropdownInput( $col, Array $props = [], Array $options, $value = null);
		//  $this->createBooleanInput( $col, Array $props = [], $value = null);
		// 	$this->createTextareaInput( $col, Array $props = [], Array $attrs= [] );
		//	$this->createInput( $col, $type, Array $props = [] , Array $attrs= [], $value = null );
		//  $this->setInputValue( $col, $value );
		//  $this->setInputType( $col, $type  );
		//  $this->setInputPorps( $col, Array $props  );
		//  $this->setInputAttrs( $col, Array $attrs  );
		//  $this->addInputPorps( $col, Array $props  );
		//  $this->addInputAttrs( $col, Array $attrs  );

		//EOC
		//BOC: Setup Fixed Column (Cannot Edit Cols)
			// Example
			/*
			// type: 
			// text, textarea, password, radio, checkbox, 
			//
			// attrs:
			// - text: placeholder
			// - textarea: placeholder, rows, cols, wrap
			// - select: 

			$this->setInputType('email', 'text');
			$this->setInputPorps('email', ['required']);
			$this->setInputAttrs('email', ['placeholder'=> 'test']);
			*/
		//EOC
	}

	protected function setupCustomizeBreadAddInputs(){
		//Customize Add Inputs ( Run After Required and Hidden list)
	}

	protected function initDebugControl(){
		$this->page['browse_debug'] = 0;
		$this->page['read_debug'] = 0;
		$this->page['add_debug'] = 0;
		$this->page['update_debug'] = 0;
		$this->page['save_debug'] = 0;
		$this->page['delete_debug'] = 0;
		$this->page['destroy_debug'] = 0;
	}

	protected function getBrowseData(){
		$result = [];

		// $result = $this->sendAPI( "browseUserByAppRole",  ['app_name' => $this->target_user('app_name') ]  );
		// + Get & Validate Browse Data @Local
		// + Send API to retrieve Browse Data

		if( !isset($result->Resp) ){
			$result = $this->APIResponse( $request->all() , $this->getResp("F", "Incorrect Response Format.") );
		}
		return $result; 
	}

	protected function getReadData( $request, $target_id ){
		$result = [];
		
		// $result = $this->sendAPI( "readUserByAppRole" , ['app_name' => $this->target_user('app_name') ,'user_id' => $target_id ] );

		// + Get & Validate Read Data @Local
		// + Send API to retrieve Read Data


		$excludes = ['id','created_at', 'updated_at','deleted_at','account_id'];
		foreach($excludes as $exclude){
			unset($result->Result->$exclude);
		}

		$result->Result->resend_route = $this->registryAccessGroupKey().'.bread.'.$this->registryItem()->singular.'.resend';
		
		if( !isset($result->Resp) ){
			$result = $this->APIResponse( $request->all() , $this->getResp("F", "Incorrect Response Format.") );
		}
		return $result;
	}

	protected function postUpdateData( $request, $target_id ){
		
		$result = [];

		// + API POST Update Data

		//BOC: Set Success Hint
			$this->page['hint'] = "Update Success.";
		//EOC	
		if( !isset($result->Resp) ){
			$result = $this->APIResponse( $request->all() , $this->getResp("F", "Incorrect Response Format.") );
		}
		return $result;
	}

	protected function postSaveData( $request ){
		$result = [];

		$request_param = $request->all();
		$target_app_name = $this->target_user('app_name'); // or Appname
	
		//BOC: validate
			$rules = [
		      	'email' => 'required',
			];
			$message = [];
			$validator = Validator::make($request_param, $rules, $message);
			if($validator->fails()) {
				$result = $this->APIResponse( $request->all() , $this->getResp("F", $validator->errors()->first()) );
			  	return $result;
			}
		//EOC
		//BOC
			// + API POST INVITE Data (INVITE)
			// $result = $this->sendAPI( "sendWithAccRegisterToken", [
			// 									'email' => $request->email, 
			// 									'app_name' => $target_app_name,
			// 									'send_email' => true
			// ]);

			if($result->Resp->Status == 'F' ) {
		        $result = $this->APIResponse( $request->all() , $this->getResp("F", $result->Resp->FailedReason ) );
				return $result;
		    }
	    //EOC
	    //BOC: Create Local Related User Data
		   
	    //EOC
		//BOC: Return Success
		    $this->page['bread_success'] = "Register Link: ". isset($result->Result->link) ? $result->Result->link: "" ;
		    $this->page['hint'] = 'Email Link Sent';
		//EOC
	    if( !isset($result->Resp) ){
			$result = $this->APIResponse( $request->all() , $this->getResp("F", "Incorrect Response Format.") );
		}
		return $result;
	}

	protected function postDestroyData( $request, $target_id ){
		$result = [];

		$result = $this->APIResponse( $request->all() , $this->getResp("F", "Delete Function Not Available.") );
		if( !isset($result->Resp) ){
			$result = $this->APIResponse( $request->all() , $this->getResp("F", "Incorrect Response Format.") );
		}
		return $result;
	}
	
	public function resend(Request $request){
		$result = [];
		$request_param = $request->all();
		$target_app_name = $this->target_user('app_name'); // or Appname

		//BOC: validate
			$rules = [
		      	'email' => 'required',
			];
			$message = [];
			$validator = Validator::make($request_param, $rules, $message);
			if($validator->fails()) {
				$result = $this->APIResponse( $request->all() , $this->getResp("F", $validator->errors()->first()) );
			  	return $result;
			}
		//EOC
		//BOC

			// + API POST Resend Register Token
			// $result = $this->sendAPI( "RegisterTokenResend", [
			// 									'email' => $request->email, 
			// 									'app_name' => $target_app_name,
			// 									'send_email' => true
			// ]);

			if($result->Resp->Status == 'F' ) {
		        $result = $this->APIResponse( $request->all() , $this->getResp("F", $result->Resp->FailedReason ) );
				return $result;
		    }
	    //EOC
		//BOC: Return Success
		    $this->page['bread_success'] = "Register Link: ". isset($result->Result->link) ? $result->Result->link: "" ;
		    $this->page['hint'] = 'Email Link Sent';
		//EOC
	    if( !isset($result->Resp) ){
			$result = $this->APIResponse( $request->all() , $this->getResp("F", "Incorrect Response Format.") );
		}
		
		return redirect()->back()->with('hint', $this->page['hint'])
                               ->with('bread_success', $this->page['bread_success'] );
	}
}
