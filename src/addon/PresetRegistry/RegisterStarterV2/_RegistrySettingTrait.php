<?php

namespace App\Http\Controllers\User\RegistryCompany;

trait _RegistrySettingTrait
{
	protected function target_user( $key ){
		$user_target_setting = [
			'app_name' => env('REGISTRY_TARGET_APP_NAME'), 	// Application Name for User belong 
			'role_key' => env('REGISTRY_TARGET_ROLE_KEY'),	// Role Key for User Belong
		]; 

		return $user_target_setting[$key];
	}

	protected function registryItem(){
		return (object)[
			'singular' => 'item',
			'plural' => 'items',
			'name' => 'Item',
		];
	}

	protected function registryAccessGroupKey(){
		return 'primary';
	}
}
