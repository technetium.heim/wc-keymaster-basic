<?php

namespace App\Http\Controllers\User\GuardRegistry;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Keymaster\foundation\APIRemoteAuthTrait;

class SendRegisterTokenController extends CradleController
{
	use APIRemoteAuthTrait;
	use _RegistrySettingTrait;

	protected $page;
	protected $redirectTo = '/';
	  
	public function index()
	{
		$this->page['title'] = ucwords(config('app.name'));
	    $this->page['meta']['description'] = ucwords(config('app.name'));
	    $this->page['item'] = $this->registryItem()->name;
	    $this->page['route_send_token'] = route($this->routeSendToken());
	    return view( $this->getView('send.register.token.index') ,$this->page);

	}

	// Create and Send Token
	// public function send(Request $request){

	// 	$request_param = $request->all();
	// 	$request_param['app_id'] = 2; // or Appname
		
	// 	$result = $this->sendAPI( "sendRegisterToken", $request_param );

	// 	if($result->Resp->Status == 'F' ) {
	//       return redirect()->back()
	//         ->withErrors( $result->Resp->FailedReason );
	//     }

	// 	return redirect()->back()->with('onsuccess', $result->Resp->FailedReason );
	// }

	// Create Token & request wooyoo to send email
	public function send(Request $request){
		$request_param = $request->all();
		$request_param['app_name'] = $this->target_user('app_name'); // or Appname
		
		$result = $this->sendAPI( "createRegisterLink", $request_param );

		if($result->Resp->Status == 'F' ) {
	      return redirect()->back()
	        ->withErrors( $result->Resp->FailedReason );
	    }

	    // check link
	    if( $result->Result->register_link ) {
	    	// request for AWS to send Email ( Waiting for AWS API)
	    	return redirect()->back()->with('onsuccess', 'Register Token Created. Email Not send (wait for AWS API). Link -> '.$result->Result->register_link );
	    }

		return redirect()->back()->with('onsuccess', 'Registration Email is sent.' );
	}
}
