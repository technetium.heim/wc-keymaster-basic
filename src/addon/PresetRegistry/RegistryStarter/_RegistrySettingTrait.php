<?php

namespace App\Http\Controllers\User\GuardRegistry;

trait _RegistrySettingTrait
{
	protected function target_user( $key ){
		$user_target_setting = [
			'app_name' => 'WooYoo Guard',
			'role_key' => 'wooyoo-guard',
		]; 

		return $user_target_setting[$key];
	}

	protected function getView( $key ){
		$view = [
			'send.register.token.index' => 'user/pages/preset/registry_send',
		]; 

		return $view[$key];
	}

	protected function registryItem(){
		return (object)[
			'singular' => 'guard',
			'plural' => 'guards',
			'name' => 'Guard',
		];
	}

	protected function registryAccessGroupKey(){
		return 'primary';
	}
	
	protected function routeSendToken(){
		return $this->registryAccessGroupKey().'.preset.registry_'.$this->registryItem()->singular.'_token.send';
	}
}
