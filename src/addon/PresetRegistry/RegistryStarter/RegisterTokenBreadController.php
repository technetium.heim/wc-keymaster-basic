<?php

namespace App\Http\Controllers\User\GuardRegistry;

use App\Http\Controllers\CradleController;
use Keymaster\addon\ManageBread\foundation\KeymasterBreadTrait;

class RegisterTokenBreadController extends CradleController
{
	use KeymasterBreadTrait;
	use _RegistrySettingTrait;

	protected $page;
	protected $redirectTo = '/';
	
	/// Settings
	protected function setupBreadTable(){
		// Table Column
		$this->bread['table_cols'] = ['email', 'register_token'];
		
		// Table Item
		$this->bread['table_item_singular'] = 'token';
		$this->bread['table_item_plural'] = 'tokens';
		$this->bread['table_name'] = $this->registryItem()->name.' Register Tokens';

		// Show Action Buttons 
		$this->bread['action_read'] = false;
		$this->bread['action_edit'] = false;
		$this->bread['action_add'] = false;
		$this->bread['action_delete'] = false;

		// Action Route
		$user_group = $this->registryAccessGroupKey();
		$item  = 'registry_'.$this->registryItem()->singular.'_token';
		$this->bread['route_index'] = route( $user_group.'.bread.'.$item.'.index');
		$this->bread['route_browse'] = route($user_group.'.bread.'.$item.'.browse');
		$this->bread['route_read'] = route($user_group.'.bread.'.$item.'.read');
		$this->bread['route_edit'] = route($user_group.'.bread.'.$item.'.read');
		$this->bread['route_add'] = route($user_group.'.bread.'.$item.'.read');
		$this->bread['route_delete'] = route($user_group.'.bread.'.$item.'.read');
	}

	protected function setupViewPagePath(){
		// View Path to blade file for pages
		$this->view['browse'] = 'user/pages/preset/bread_main:browse';
		$this->view['read'] = 'user/pages/preset/bread_main:read';
		$this->view['edit'] = '';
		$this->view['add'] = '';
	}

	protected function getBrowseData($request){
		return  $this->sendAPI( "browseRegisterTokenByApp", ['app_name' => $this->target_user('app_name') ] );
	}

	protected function getReadData($request){
		return false;
	}
}
