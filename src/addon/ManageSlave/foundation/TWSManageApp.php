<?php

namespace Keymaster\addon\ManageSlave\foundation;

use GuzzleHttp\Client;
use WCPassportApi\models\PassportApi;
use WCPassportApi\supports\PassportToken;
use Keymaster\Models\RemoteAuth;
use Auth;
use Log;

class TWSManageApp
{
    // To setup default data to send API
    protected function default_data(){
        $param = RemoteAuth::select(
                                    'email',
                                    'role_key',
                                    'remote_account_id',
                                    'remote_account_token'
                                )->find( Auth::guard('web_remote')->user()->id )->toArray();
        return $param;
    }

    protected function uri_base(){      
        // **to add: remove '/' at the end of url for standard
        if($this->AuthConfig()['api_connect'] == 'passport-api'){
            return $this->passport()->uri."api/";
        }
        return $this->AuthConfig()['api_url'];
    }

    protected function bearerToken(){
        if($this->AuthConfig()['api_connect'] == 'passport-api'){
            return $this->passport()->token;
        }
        return $this->AuthConfig()['api_token'];
    }

    public function sendEmptyAPI( $target, $param = [] , $method="POST" )
    {
        return $this->sendAPI( $target, $param, $method, false);
    }

    public function sendAPI( $target, $param = [] , $method="POST", $default = true )
    {
        //set default
            $status = 'pending';
            $origin_message = '<b>Internal Server Error</b><br>Please contact admin.';
            $response = (object) array('status'=>$status, 'message'=>$origin_message);

            $this->page['result'] = '';
            $this->page['json'] = '';

        
        //set request
            $r['uri'] = $this->uri_base().$target;
            $r['headers'] = array(
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$this->bearerToken(),
                'Content-Type' => 'application/json',
            );

            if( $default == true ){
                //set params
                $r['params'] = $this->default_data();
            }else {
                $r['params'] = array();
            }
            
            if( !empty($param) ){
                $r['params'] = array_merge( $r['params'] , $param );
            }

        //retrieve info
            $http = new Client;

            $response = $http->request( $method ,$r['uri'], [
                'headers' => $r['headers'],
                'query' => "",
                'json' =>  $r['params'],
                'verify' => false
            ]);

            $sessionidx = $response->getHeader("Set-Cookie");

            if( $sessionidx ){
                $sessionidx = str_replace('SESSIONIDX=', '',$sessionidx[0]);
                $sessionidx = str_replace('; path=/; HttpOnly', '',$sessionidx);
                $this->idx = $sessionidx;
            }

            $data = json_decode($response->getBody());


            return $data;
    }

    protected function AuthConfig(){
        if( config( 'manageslave') == null ) Log::debug('Config file not existed. ManageSlave not Initiated.') ;
        return config( 'manageslave');
    }

    protected function passport(){
        $passport_api_name = $this->AuthConfig()['api_name'];

        // return $passport = PassportApi::where('name', $passport_api_name )->first();
        return $passport = PassportToken::getTokenLocal($passport_api_name);
    }
}