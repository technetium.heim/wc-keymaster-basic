<?php
/*
    |--------------------------------------------------------------------------
    | Manage Slave Settings
    |--------------------------------------------------------------------------
    |'group_key' => [
	|		'user_dir' => 'user',													// view folder directory		
	|		'prefix' => '',															// prefix in route
	|		'public' => env('PRIMARY_REMOTE_PUBLIC', false ),						// register require register token
	|		'role_key' => env('PRIMARY_REMOTE_ROLE_KEY', '' ), 						// role key to link to gatekeeper 
	|		'api_connect' => env('PRIMARY_REMOTE_API_CONNECT', 'passport-api' ),	// manual/ passport-api
	|		'api_name' => env('PRIMARY_REMOTE_API_NAME', '' ), 						// passport-api name
	|		'api_url' => env('PRIMARY_REMOTE_API_URL', '' ),						// manual - url
	|		'api_token' => env('PRIMARY_REMOTE_API_TOKEN', '' ),					// manual token
			
			// bottom options only required to add for customize
			'authenticated_redirect' => 'primary.user.dashboard',					// redirect route if logged in
			'unauthenticated_redirect' => 'primary.user.landing',					// redirect route if not logged in
			'login_after_register' => true,											// should login after register
	|	], 
    | 	



    */

return [
		'api_connect' => env('MANAGE_MASTER_API_CONNECT', 'passport-api' ),	// manual/ passport-api
		'api_name' => env('MANAGE_MASTER_API_NAME', '' ), 					// passport-api name
		'api_url' => env('MANAGE_MASTER_API_URL', '' ),						// manual - url
		'api_token' => env('MANAGE_MASTER_API_TOKEN', '' ),	
];
