<?php

namespace Keymaster\addon\ManageBread\foundation;

use Illuminate\Http\Request;
use Carbon\Carbon;
// use App\Http\Controllers\APIRemoteAuthTrait;
use Keymaster\foundation\APIRemoteAuthTrait;

trait KeymasterBreadTrait
{
    use APIRemoteAuthTrait;

    protected $bread;

    protected $setting;

    protected $view;


    // ################## Hidden Funtions #####################
    // ** Change when custom view design for bread 
    protected function setupBreadViewPath(){
        $this->view_browse = '';
        $this->view_read = '';
        $this->view_edit = '';
        $this->view_add = '';
        $this->view_delete = '';
    }

    protected function setupPage(){
        $this->page['title'] = ucwords(config('app.name'));
        $this->page['meta']['description'] = ucwords(config('app.name'));
    }

    protected function initBread(){
        // Bread
        $this->setupBreadTable();
        
        // Views
        $this->setupViewPagePath();
        
        
    }



    /// MAIN FUNCTIONS
    public function index()
    {   
        $this->setupPage();
        $this->initBread();

        $path = $this->getIndexView($this->view['browse']);

        $this->page['load_page'] =  $path->load_page;
        return view(  $path->path , $this->page )->withBread( (object)$this->bread );
    }

    public function browse(Request $request)
    {
        $result = $this->getBrowseData($request);
     
        $currentPage = isset($request->current) ? (int)$request->current : 1 ;
        $rowCount = isset($request->rowCount) ? (int)$request->rowCount: 10 ;

        if($result->Resp->Status == 'F' ) {
          return redirect()->back()
            ->withErrors( $result->Resp->FailedReason );
        }

        $response_data = (object) [
            'current' => $currentPage,
            'rowCount' => $rowCount,
            'rows' =>  $result->Result,
            'total' =>  count($result->Result),
        ];   

        return response()->json($response_data);
    }

    public function read(Request $request, $id )
    {
        $this->setupPage();
        $this->initBread();

        $request->request->add([ 'read_id' =>  $id ]);
        $result = $this->getReadData($request);
  
        if($result->Resp->Status == 'F' ) {
          return redirect()->back()
            ->withErrors( $result->Resp->FailedReason );
        }
        
        $this->setBreadRows($result->Result);

        $path = $this->getIndexView($this->view['read']);

        $this->page['load_page'] =  $path->load_page;
        return view(  $path->path , $this->page )->withBread( (object)$this->bread );
    }

    protected function setBreadRows( $data ){
        $this->bread['rows'] = $data;
    }

    protected function getIndexView( $path ){
        if (strpos($path, ':') == false){
            return (object)[
                'path' => $path,
                'load_page' => null,
            ];
        }

        $path = explode(":", $path);
        return (object)[
            'path' => $path[0],
            'load_page' => $path[1],
        ];
    }
}