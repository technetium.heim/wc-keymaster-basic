<?php

namespace Keymaster\addon\ManageBread\foundation;

use Illuminate\Http\Request;
use Carbon\Carbon;
// use App\Http\Controllers\APIRemoteAuthTrait;
use Keymaster\foundation\APIRemoteAuthTrait;
use Illuminate\Pagination\LengthAwarePaginator;

trait KeymasterBreadTraitV2
{
    use APIRemoteAuthTrait;

    protected $browse_filters;
    protected $bread;
    protected $setting;
    protected $view;
    protected $form;
    protected $inputs = [] ;
    protected $hidden_inputs = [] ;

    protected $add_data = [];
    // ################## Hidden Funtions #####################
    // ** Change when custom view design for bread 
    protected function setupBreadViewPath(){
        $this->view_browse = '';
        $this->view_read = '';
        $this->view_edit = '';
        $this->view_add = '';
        $this->view_delete = '';
    }

    protected function setupPage( $page="" ){
        switch ($page) {
            case 'browse':
                if( $this->bread['title_browse'] == "default" || $this->bread['title_browse'] == "" ){
                    $title = ucwords( str_replace('_',' ', $this->bread['table_item_plural']) );
                }else{
                    $title = $this->bread['title_browse'];
                }
                break;
            case 'read':
                if( $this->bread['title_read'] == "default" || $this->bread['title_read'] == "" ){
                    $title = ucwords( str_replace('_',' ', $this->bread['table_item_singular']) );
                }else{
                    $title = $this->bread['title_read'];
                }
                break;
            case 'add':
                if( $this->bread['title_add'] == "default" || $this->bread['title_add'] == "" ){
                    $title = 'Add New '.ucwords( str_replace('_',' ', $this->bread['table_item_singular']) );
                }else{
                    $title = $this->bread['title_add'];
                }
                break;
            default:
                $title = ucwords(config('app.name'));
                break;
        }

        $this->page['title'] =  $title;
        $this->page['meta']['description'] = $title;
    }

    protected function initBread(){
        $this->page['bread_error'] = [];
        $this->page['hint'] = "";
        $this->page['bread_success'] = "";
        $this->setupBreadLayout();
        $this->setupBreadBase();
        $this->setupViewPagePath();
        $this->initDebugControl();
    }

    protected function initBreadModify(){
        $this->page['bread_error'] = [];
        $this->page['hint'] = "";
        $this->page['bread_success'] = "";
        $this->setupBreadLayout();
        $this->initDebugControl();
    }

    public function browse( Request $request )
    {   
        //BOC:Initiate Bread Table Setting 
            $this->initBread();
        //EOC
        //BOC: Page -> Set Title
            $this->setupPage('browse');
        //EOC
        //BOC: Page -> Set Route Back
            $this->page['route_back'] = $this->bread['browse_route_back'];
        //EOC
        //BOC: Table -> Prepare Column Size Class
            $this->prepBrowseCols();
        //EOC
        //BOC: Select View
            $path = $this->view['browse'];
        //EOC
        //BOC: Data -> Get Browse Data    
            $result = $this->getBrowseData( $request );
        //EOC
        //BOC: Data -> Prepare Error
            $this->page['Result_Response']  = $result;
            if( isset($result->Resp) ){
                if($result->Resp->Status == 'F' ) {
                    $this->page['data'] =  [];
                    $error = $result->Resp->FailedReason? $result->Resp->FailedReason: "Error.";
                    array_push( $this->page['bread_error'] , $error);
                    if( $this->page['browse_debug'] ) dd($this->page);
                    return view(  $path , $this->page );
                }
            }
        //EOC
        //BOC: Data -> Select Data
            if( isset($result->Result) ){
                $data = $result->Result;
            }else{
                $data = $result;
            }
        //EOC
        //BOC: Data Comes with external Filter Data
            if( isset( $data->browse_data) ){
                if( isset($data->browse_filters) ){
                    $this->browse_filters = $data->browse_filters;
                }
                $data = $data->browse_data;
            }
        //EOC
        //BOC: Add Browse Filter Setting
            $this->bread['filter_setting'] = $this->setupBreadBrowseFilter();
        //EOC
        //BOC: Set Bread Table data to Page
             $this->page['bread'] = (object)$this->bread;
        //EOC 
        // Recreate Paginator object
        $is_paginate = false;

        if( isset( $data->current_page ) ){    
            $is_paginate = true;
            $data = new LengthAwarePaginator($data->data, $data->total , $data->per_page , $data->current_page, ['path' => $request->url() ]);
        }

        //BOC: Data -> Add Read Route to browse data
            if( $this->bread['action_read'] ){
                $target_col_name = $this->bread['read_target_col_name'];
                
                $collection_data = $is_paginate ? (object)$data->items() : (object)$data;

                foreach ($collection_data as $d ){
                    $d->route_read = route( $this->bread['route_read'], ['id' => $d->$target_col_name  ] );
                }
            }
        //EOC
        //BOC: Set Browse Data To Page
            $this->page['data'] = $data;
        //EOC 
        //BOC: Prepare Debug
            if( $this->page['browse_debug'] ){
                $this->DevDebug(); 
                dd($this->page);
            }
        //EOC
        //BOC:
            if( $request->sort || $request->page || $request->sort_order || $request->filters){
                $request->session()->put('bread_browse_back', (object)[ 'base_url'=> $request->url(), 'full_url' => $request->fullUrl()]);
            }else{
                $request->session()->forget('bread_browse_back');
            }
        //EOC
        return view(  $path , $this->page );
    }

    public function read(Request $request, $id )
    {
        //BOC:Initiate Bread Table Setting 
            $this->initBread();
        //EOC
        //BOC: Page -> Set Title
            $this->setupPage('read');
        //EOC
        //BOC: Page -> Set Route Back
            $this->page['route_back'] = route( $this->bread['route_browse'] );

            if( $request->session()->has('bread_browse_back') ){
                $bread_browse_back = $request->session()->get('bread_browse_back');
                if( isset($bread_browse_back->base_url) ){
                    if( $bread_browse_back->base_url == $this->page['route_back'] ){
                        $this->page['route_back'] = $bread_browse_back->full_url;
                    }
                }
            }
        //EOC
        //BOC: Select View
            $path = $this->view['read'];
        //EOC   
        //BOC: Set Edit Form & input
            if( $this->bread['action_edit'] ){
                $this->initBreadInputs('edit');
                $this->bread['form'] = $this->form['edit'];
                $this->bread['inputs'] = (object)$this->inputs; 
                $this->bread['hidden_inputs'] = (object)$this->hidden_inputs; 
            }            
        //EOC 
        //BOC: Set bread data to page
            $this->page['bread'] = (object)$this->bread;
        //EOC
        //BOC: Data -> Get Read Data       
            $result = $this->getReadData($request, $id );
        //EOC
        //BOC: Data -> Prepare Error
            $this->page['Result_Response']  = $result;
            if( isset($result->Resp) ){
                if($result->Resp->Status == 'F' ) {
                    $this->page['data'] =  [];
                    $error = $result->Resp->FailedReason? $result->Resp->FailedReason: "Error.";
                    array_push( $this->page['bread_error'] , $error);
                    if( $this->page['read_debug'] ) dd($this->page);
                    return view(  $path , $this->page );
                }
            }
        //EOC
        //BOC: Data -> Select Data
            if( isset($result->Result) ){
                $data = $result->Result;
            }else{
                $data = $result;
            }
        //EOC
        //BOC: Data -> Set Data To Page
            $this->page['data'] = $data;
            $this->addAdditionalData();
        //EOC
        //BOC: Prepare Debug
            if( $this->page['read_debug'] ) {
                $this->DevDebug(); 
                dd($this->page);
            }
        //EOC
        return view(  $path , $this->page );
    }

    public function add(Request $request )
    {
        //BOC:Initiate Bread Table Setting 
            $this->initBread();
        //EOC
        //BOC: Page -> Set Title
            $this->setupPage('add');
        //EOC
        //BOC: Page -> Set Route Back
            $this->page['route_back'] = route( $this->bread['route_browse'] );

            if( $request->session()->has('bread_browse_back') ){
                $bread_browse_back = $request->session()->get('bread_browse_back');
                if( isset($bread_browse_back->base_url) ){
                    if( $bread_browse_back->base_url == $this->page['route_back'] ){
                        $this->page['route_back'] = $bread_browse_back->full_url;
                    }
                }
            }
        //EOC
        //BOC: Select View
            $path = $this->view['add'];
        //EOC
        //BOC: Set Edit Form & input
            if( $this->bread['action_add'] ){
                $this->initBreadInputs('add');
                $this->bread['form'] = $this->form['add'];
                $this->bread['inputs'] = (object)$this->inputs; 
                $this->bread['hidden_inputs'] = (object)$this->hidden_inputs; 
            }            
        //EOC
        //BOC: Set bread data to page
            $this->page['bread'] = (object)$this->bread;
        //EOC
        //BOC: Data -> Get Add Data       
            $result = $this->getAddData();
        //EOC
        //BOC: Data -> Prepare Error
            $this->page['Result_Response']  = $result;
            if( isset($result->Resp) ){
                if($result->Resp->Status == 'F' ) {
                    $this->page['data'] =  [];
                    $error = $result->Resp->FailedReason? $result->Resp->FailedReason: "Error.";
                    array_push( $this->page['bread_error'] , $error);
                    if( $this->page['add_debug'] ) dd($this->page);
                    return view(  $path , $this->page );
                }
            }
        //EOC
        //BOC: Data -> Select Data
            if( isset($result->Result) ){
                $data = $result->Result;
            }else{
                $data = $result;
            }
        //EOC
        //BOC: Data -> Set Data To Page
            $this->page['data'] = $data;
            $this->addAdditionalData();
        //EOC
        //BOC: Prepare Debug
            if( $this->page['add_debug'] ) {
                $this->DevDebug(); 
                dd($this->page);
            }
        //EOC
        return view(  $path , $this->page );
    }

    // public function edit(Request $request, $id)
    // {
    //     return;
    // }

    // public function delete(Request $request, $id)
    // {
    //     return;
    // }

    public function save(Request $request)
    {
        $this->initBreadModify();
        //BOC: Data -> Send Update Data       
            $result = $this->postSaveData($request);
        //EOC
        //BOC: Data -> Prepare Error
            $this->page['Result_Response']  = $result;
            if( isset($result->Resp) ){
                if($result->Resp->Status == 'F' ) {
                    $this->page['data'] =  [];
                    $error = $result->Resp->FailedReason? $result->Resp->FailedReason: "Error.";
                    array_push( $this->page['bread_error'] , $error);
                    if( $this->page['save_debug'] ) dd($this->page);
                    return redirect()->back()->with('bread_error', $this->page['bread_error'] )
                                            ->withInput($request->input())
                                            ->with('form_default_state', 'add');
                }
            }
        //EOC
        //BOC: Prepare Debug
            if( $this->page['save_debug'] ){
                $this->DevDebug(); 
                dd($this->page);
            }
        //EOC
        //BOC: Success
            return redirect()->back()->with('hint', $this->page['hint'])
                                    ->with('bread_success', $this->page['bread_success'] );            
        //EOC
    }

    public function update(Request $request, $id)
    {
        $this->initBreadModify();
        //BOC: Data -> Send Update Data       
            $result = $this->postUpdateData($request, $id );
        //EOC
        //BOC: Data -> Prepare Error
            $this->page['Result_Response']  = $result;
            if( isset($result->Resp) ){
                if($result->Resp->Status == 'F' ) {
                    $this->page['data'] =  [];
                    $error = $result->Resp->FailedReason? $result->Resp->FailedReason: "Error.";
                    array_push( $this->page['bread_error'] , $error);
                    if( $this->page['update_debug'] ) dd($this->page);
                    return redirect()->back()->with('bread_error', $this->page['bread_error'] )
                                            ->withInput($request->input())
                                            ->with('form_default_state', 'edit');
                }
            }
        //EOC
        //BOC: Prepare Debug
            if( $this->page['update_debug'] ){
                $this->DevDebug(); 
                dd($this->page);
            }
        //EOC
        //BOC: Success
            return redirect()->back()->with('hint', $this->page['hint'])
                                    ->with('bread_success', $this->page['bread_success'] );            
        //EOC
    }

    public function destroy(Request $request, $id)
    {
        $this->initBread();
        $this->page['hint'] = "Delete Success";
        //BOC: Data -> Send Update Data       
            $result = $this->postDestroyData($request, $id );
        //EOC
        //BOC: Data -> Prepare Error
            $this->page['Result_Response']  = $result;
            if( isset($result->Resp) ){
                if($result->Resp->Status == 'F' ) {
                    $this->page['data'] =  [];
                    $error = $result->Resp->FailedReason? $result->Resp->FailedReason: "Error.";
                    array_push( $this->page['bread_error'] , $error);
                    if( $this->page['destroy_debug'] ) dd($this->page);
                    return redirect()->back()->with('bread_error', $this->page['bread_error'] );
                }
            }
        //EOC
        //BOC: Prepare Debug
            if( $this->page['destroy_debug'] ){
                $this->DevDebug(); 
                dd($this->page);
            }
        //EOC
        //BOC: Success
            return redirect()->route( $this->bread['route_browse'] )->with('hint', $this->page['hint']);           
        //EOC
    }

    protected function getAddData(){
        return [];
    }

    protected function constructBreadInputs( $action_type ){
        $required_cols  = [];
        $hidden_col= [];

        if( $action_type == 'edit' ){
            $required_cols= $this->bread['edit_cols_required'];
            $hidden_cols= $this->bread['edit_cols_hidden'];
        }    
        if( $action_type == 'add' ){
            $required_cols= $this->bread['add_cols_required'];
            $hidden_cols= $this->bread['add_cols_hidden'];
        }    
        
        //BOC: Setup Required Inputs
        if( !empty($required_cols) ){
            foreach( $required_cols as $required_col ){
                $this->addInputPorps( $required_col , ['required'] );
            } 
        }
        //EOC
        //BOC: Setup Hidden Inputs
        if(!empty( $hidden_cols ) ){
            foreach( $hidden_cols as $hidden_col ){
                $this->setInputType( $hidden_col , 'hidden' );
            } 
        } 
    }

    protected function constructCustomizeBreadInputs( $action_type ){
        return;
    }

    protected function prepBrowseCols(){
        
        $col_count = count( $this->bread['browse_cols'] );
        $num =  (int) (12 / $col_count);
        $remain = 12 % $col_count;
                    
        $cols_size =[]; 
        for( $i = 0; $i < $col_count; $i++ ){
            $sm = $remain > 0 ? $num + 1: $num;
            array_push($cols_size, $sm);
            $remain --;
        }

        $cols_sm = $this->bread['browse_cols_sm'];
        if( empty($cols_sm) ) $cols_sm = $cols_size;
        $cols_xs = $this->bread['browse_cols_xs'];
        if( empty($cols_xs) ) $cols_xs = $cols_size;

        //BOC: Prepare Cols Class
            $cols_class = [];
            for( $i = 0; $i < $col_count; $i++ ){
                $sm_class =  $cols_sm[$i] == 0? 'hidden-sm': 'col-sm-'.$cols_sm[$i];
                $xs_class =  $cols_xs[$i] == 0? 'hidden-xs': 'col-xs-'.$cols_xs[$i];
                
                array_push($cols_class, $xs_class." ".$sm_class );
            }
        //EOC
        $this->bread['cols_class'] =  $cols_class;
    }

    protected function initBreadInputs( $action_type ){
        $this->setupBreadInputs( $action_type );
        

        if( $action_type == 'edit'){
            $this->setupCustomizeBreadEditInputs();
        }
        if( $action_type == 'add'){
            $this->setupCustomizeBreadAddInputs();
        }        

        $this->constructBreadInputs( $action_type );
        
        foreach( $this->inputs as $key => $input ){     
            if( $action_type == 'edit'){
                //Filter Inputs by Hidden        
                if( $input->type == "hidden" ){
                    // Add Col name into hidden cols array
                    if( !in_array( $key , $this->bread['edit_cols_hidden'] ) ){
                        array_push( $this->bread['edit_cols_hidden'] , $key );
                    }
                    // Remove Col name from normal arrays
                    if( in_array( $key , $this->bread['read_cols'] ) ){
                        unset($this->bread['read_cols'][ array_search( $key , $this->bread['read_cols'] ) ]);
                    }
                }else{
                    // Add Col name into normal cols array
                    if( !in_array( $key , $this->bread['read_cols'] ) ){
                        array_push( $this->bread['read_cols'] , $key );
                    }
                    // Remove Col name from hidden arrays
                    if( in_array( $key , $this->bread['edit_cols_hidden'] ) ){
                        unset($this->bread['edit_cols_hidden'][ array_search( $key , $this->bread['edit_cols_hidden'] ) ]);
                    }
                }
            }
            if( $action_type == 'add'){
                //Filter Inputs by Hidden        
                if( $input->type == "hidden" ){
                    // Add Col name into hidden cols array
                    if( !in_array( $key , $this->bread['add_cols_hidden'] ) ){
                        array_push( $this->bread[''] , $key );
                    }
                     // Remove Col name from normal arrays
                    if( in_array( $key , $this->bread['add_cols'] ) ){
                        unset($this->bread['add_cols'][ array_search( $key , $this->bread['add_cols'] ) ]);
                    }
                }else{
                    // Add Col name into hidden cols array
                    if( !in_array( $key , $this->bread['add_cols'] ) ){
                        array_push( $this->bread['add_cols'] , $key );
                    }
                    // Remove Col name from normal arrays
                    if( in_array( $key , $this->bread['add_cols_hidden'] ) ){
                        unset($this->bread['add_cols_hidden'][ array_search( $key , $this->bread['add_cols_hidden'] ) ]);
                    }
                }
            }       
        }

        array_unique($this->bread['read_cols']);
        array_unique($this->bread['add_cols']);
        array_unique($this->bread['edit_cols_hidden']);
        array_unique($this->bread['add_cols_hidden']);
    }

    protected function addAdditionalData(){
        if( !empty($this->add_data) ){
            foreach( $this->add_data as $key => $val){
                if(empty( $this->page['data'] )){
                    $this->page['data'] = (object)[ $key => $val];    
                }else{
                    $this->page['data']->$key = $val;
                }      
            }
        }
    }

    protected function createHiddenInput( $col, $value = null ){
        $this->createInputObject($col);
        $this->inputs[$col]->type = "hidden";
        $this->inputs[$col]->props = [];
        $this->inputs[$col]->attrs = [];

        if( $value != null){
            $this->setInputValue( $col, $value );
        }
    }

    protected function createTextareaInput( $col, Array $props = [], Array $attrs= [], $value = null){
        $this->createInputObject($col);
        $this->inputs[$col]->type = "textarea";
        $this->inputs[$col]->props = $props;
        $this->inputs[$col]->attrs = $attrs;

        if( $value != null){
            $this->setInputValue( $col, $value );
        }
    }

    protected function createDropdownInput( $col, Array $props = [], Array $options, $value = null){
        $this->createInputObject($col);
        $this->inputs[$col]->type = "dropdown";
        $this->inputs[$col]->props = $props;
        $this->inputs[$col]->attrs = [];
        $this->inputs[$col]->options = $options;

        if( $value != null){
            $this->setInputValue( $col, $value );
        }
    }

    protected function createBooleanInput( $col, Array $props = [], $value = null){
        $this->createInputObject($col);
        $this->inputs[$col]->type = "boolean";
        $this->inputs[$col]->props = $props;
        $this->inputs[$col]->attrs = [];
        $this->inputs[$col]->options = [];

        if( $value != null){
            $this->setInputValue( $col, $value );
        }
    }

    protected function createInput( $col, $type, Array $props = [] , Array $attrs= [], $value = null ){
        $this->createInputObject($col);
        $this->inputs[$col]->type = $type;
        $this->inputs[$col]->props = $props;
        $this->inputs[$col]->attrs = $attrs;

        if( $value != null){
            $this->setInputValue( $col, $value );
        }
    }

    protected function setInputValue( $col, $value ){
        $this->add_data[$col] = $value;
    }

    protected function setInputType( $col, $type  ){
        $this->createInputObject($col);
        $this->inputs[$col]->type = $type;
    }

    protected function setInputPorps( $col, Array $props  ){
        $this->createInputObject($col);
        $this->inputs[$col]->props = $props;
    }

    protected function setInputAttrs( $col, Array $attrs  ){
        $this->createInputObject($col);
        $this->inputs[$col]->attrs = $attrs;
    }

    protected function addInputPorps( $col, Array $props  ){
        $this->createInputObject($col);
        foreach($props as $prop){
            array_push( $this->inputs[$col]->props , $prop );
        }
    }

    protected function addInputAttrs( $col, Array $attrs  ){
        $this->createInputObject($col);
        foreach($attrs as $attr){
            array_push( $this->inputs[$col]->attrs , $attr );
        }
    }

    protected function createInputObject( $col ){
        if( !isset($this->inputs[$col] ) ){
            $this->inputs[$col] = (object)[
                'type' => "",
                'props' => [],
                'attrs' => [],
            ];
        }  
    }

    protected function DevDebug( ){
        $this->page['DevDebug'] = [];
        if( $this->bread['action_edit_type'] == 'redirect' && $this->bread['route_edit'] == "" ){
            array_push( $this->page['DevDebug'] , 'action_edit_type is redirect but route_edit not Setup.');
        }
    }

    protected function KMBreadView( $key ){
        $path['browse'] = 'KMBreadView::V2.bread_browse';
        $path['read'] = 'KMBreadView::V2.bread_read';
        $path['add'] = 'KMBreadView::V2.bread_add';

        return $path[$key];
    }

    protected function KMBreadRoute( $key, $user_group = 'primary', $item, $unique = "" ){
        if($unique != "" ){
            $item = $unique.".".$item;
        }
        $route['browse'] = $user_group.'.bread.'.$item.'.browse';
        $route['read'] = $user_group.'.bread.'.$item.'.read';
        $route['add'] = $user_group.'.bread.'.$item.'.add';
        $route['save'] = $user_group.'.bread.'.$item.'.save';
        $route['update'] = $user_group.'.bread.'.$item.'.update';
        $route['edit'] = $user_group.'.bread.'.$item.'.edit';
        $route['delete'] = $user_group.'.bread.'.$item.'.delete';
        $route['destroy'] = $user_group.'.bread.'.$item.'.destroy';

        return  $route[$key];
    }

    protected function KMBreadForm( ){
        $form = 'KMBreadView::V2.bread_form';

        return $form;
    }

    protected function setupBreadLayout( ){
        $this->bread['base_layout'] = 'KMBreadView::V2._bread_layout';
    }

    protected function setupBreadBrowseFilter(){
        $filter_setting = (object)[
            'show_filter' => false, //show collapse view & button
            'with_sort' => false, //show sort view
            'with_sort_order' => false, //show sort > sort order input
            'with_sort_option' => false, //show sort > sort by input
            'sort_options' => [
            ],
            'with_filter_option' => false, //show filter view
            'filter_options' => [
            ],
            'filter_view' => 'KMBreadView::V2.bread_browse_filter',
            
        ];

        return $filter_setting;
    }
}
