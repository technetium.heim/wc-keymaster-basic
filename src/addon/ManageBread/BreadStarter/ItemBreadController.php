<?php

namespace ; /*SETUP YOUR NAMESPACE*/

use App\Http\Controllers\CradleController;
use Keymaster\addon\ManageBread\foundation\KeymasterBreadTrait;

class ItemBreadController extends CradleController
{
	use KeymasterBreadTrait;

	protected $page;
	protected $redirectTo = '/';
	
	/// Settings
	protected function setupBreadTable(){
		// Table Column
		$this->bread['table_cols'] = [ 'id', 'name' ]; /*To Edit*/
		
		// Table Item
		$this->bread['table_item_singular'] = 'item'; /*To Edit*/
		$this->bread['table_item_plural'] = 'items'; /*To Edit*/
		$this->bread['table_name'] = 'Item'; /*To Edit*/

		// Show Action Buttons 
		$this->bread['action_read'] = true;
		$this->bread['action_edit'] = false;
		$this->bread['action_add'] = false;
		$this->bread['action_delete'] = false;

		// Action Route
		$user_group = 'primary'; 
		$item  = 'item'; /*To Edit*/
		$this->bread['route_index'] = route( $user_group.'.bread.'.$item.'.index');
		$this->bread['route_browse'] = route($user_group.'.bread.'.$item.'.browse');
		$this->bread['route_read'] = route($user_group.'.bread.'.$item.'.read');
		$this->bread['route_edit'] = route($user_group.'.bread.'.$item.'.read');
		$this->bread['route_add'] = route($user_group.'.bread.'.$item.'.read');
		$this->bread['route_delete'] = route($user_group.'.bread.'.$item.'.read');
	}

	protected function setupViewPagePath(){
		// View Path to blade file for pages
		$this->view['browse'] = 'user/pages/preset/registry_bread_main:browse';
		$this->view['read'] = 'user/pages/preset/registry_bread_main:read';
		$this->view['edit'] = '';
		$this->view['add'] = '';
	}

	protected function getBrowseData($request){
		/*Browse API to get Data*/
		/*Data in structure of Request, Resp, Result */
		/*Return data item ID column name must be "id" for Read To work Example: $data->id = "1"; */ 
		/*To Edit*/
		$data = [];

		return  $data;
	}

	protected function getReadData($request){
		/*To Edit*/
		/*Data in structure of Request, Resp, Result */
		/*ID for read : $request->read_id */  
		return false;
	}
}
