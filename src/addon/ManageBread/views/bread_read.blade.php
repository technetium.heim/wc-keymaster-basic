@php
    $rows = $bread->rows;

    $route_index = $bread->route_index;
@endphp

<div>                 
    @component('WCView::general.components.panels.main')
    @slot('title')
        {{--<!-- Header -->--}}
       <div>
            <span>Read</span>

            <a href="{{ $route_index }}" class="pull-right"> Back </a>
       </div>
    @endslot
    @slot('content')
        @foreach($rows as $key => $value)
            <div class="row">
            {{ $key }} : {{ $value }}
            </div>
        @endforeach
       

    @endslot
    @endcomponent     
</div>

