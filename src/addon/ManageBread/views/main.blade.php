@php
    //default
    $load_page  = isset( $load_page )? $load_page: null;  

    if($load_page == 'browse') $view_path = 'KMBreadView::bread_browse'; 
    elseif($load_page == 'read') $view_path = 'KMBreadView::bread_read'; 
    
@endphp

@if( $load_page == null )
    <div class="alert alert-danger">
        Dev Error: Please Add "load_page" variable in Bread Controller
    </div>
@else
    @include ( $view_path ) 
@endif