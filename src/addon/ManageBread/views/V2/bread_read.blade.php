@extends($bread->base_layout)

@section('subheader')
  <div class="height-50 row border-bottom hidden-print">
    <div class="col-xs-12">
      <div class="pull-left">
        <a class="btn" href="{{$route_back}}">
          <i class="icon-50 material-icons text-black">arrow_back</i>
        </a>
      </div>
    </div>
  </div>
  <div class="height-50 row border-bottom">
    <div class="col-xs-12">
      <div class="textbox-50 text-18 text-black text-left">{{$title}}</div>
    </div>
  </div>
@endsection

@section('content_class','bg-null padding-bottom-100 text-left text-black')
@section('content_height','calc(100vh - 150px)')
@section('content')

  @if( !empty( $bread_error) || session('bread_error') )
    @php
      if( session('bread_error') ) $bread_error = session('bread_error');
    @endphp
    <div class="text-left alert alert-danger no-margin">
      <span class="help-block">
        @foreach($bread_error as $e)
        <strong>{{ $e }}</strong>
        <br>
        @endforeach
      </span>
    </div>
  @endif
  @if( session('bread_success') )
    <div class="text-left alert alert-success no-margin">
      <span class="help-block">
        <strong>{!! session('bread_success') !!}</strong>        
      </span>
    </div>
  @endif
  @php
    // Setup Form Parameter
    $component_id = 'edit-read';
    $form_id = 'form-km-bread-edit';
    $bread_form_action_type = "read";
  @endphp
  @if( $data )
    <div id="{{ $component_id }}" class="row">
      <div class="col-xs-12">
        <!-- panel -->
        <div class="row margin-10 border bg-white">
          <div class="col-xs-12">
            <!-- panel header -->
            <div class="row border-bottom height-40">
              <div class="col-xs-6">
                <div class="textbox-40">Overview</div>
              </div>
              <div class="col-xs-6 hidden-print">
                @if($bread->action_delete)
                  <div class="pull-right">
                    @if( $bread->button_delete_type == 'redirect')
                      <a class="btn" href="{{ route('route_delete') }}">
                    @else
                      <a id="delete-button" class="btn">
                    @endif
                        @if( $bread->button_delete == 'default' || $bread->button_delete == '' )
                          <i class="icon-40 material-icons text-primary">delete</i>
                        @else
                          <div class="padding-5">
                            <div class="textbox-30">{{ $bread->button_delete }}</div>
                          </div>
                        @endif
                      </a>
                  </div>
                @endif

                @if($bread->action_edit)
                  <div class="pull-right">
                    @if( $bread->button_edit_type == 'redirect')
                      <a class="btn" href="{{ route('route_edit') }}">
                    @else
                      <a id="edit-button" class="btn">
                    @endif
                        @if( $bread->button_edit == 'default' || $bread->button_edit == '')
                          <i class="icon-40 material-icons text-primary">edit</i>
                        @else
                          <div class="padding-5">
                            <div class="textbox-30">{{ $bread->button_edit }}</div>
                          </div>
                        @endif
                      </a>
                  </div>
                @endif
              </div>
            </div>
            <!-- panel body -->
            <div class="row">
              <div class="col-xs-12">
                @if( $bread->action_edit && $bread->form )
                  @include($bread->form)
                @else
                  @foreach($bread->read_cols as $col)
                      <div class="row height-30">
                        <div class="col-xs-12 col-sm-4">
                          <div class="textbox-30">{{ucwords(str_replace('_', ' ', $col))}}</div>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                          <div class="textbox-30 text-sub">
                            @if( isset($data->$col) )
                                {{ $data->$col }}
                            @endif
                          </div>
                        </div>
                      </div>
                  @endforeach
                @endif
              </div>
            </div>
            <!-- panel footer -->
            @if( $bread->action_edit )
              <div id="edit-action-button" class="row height-40 hidden">
                <hr class="no-margin">
                <a id="edit-action-cancel" class="pull-right padding-5">
                  <div class="btn textbox-30 btn-default border">
                    @if( $bread->button_update_cancel == 'default' || $bread->button_update_cancel == '')
                      Cancel
                    @else
                      {{ $bread->button_update_cancel }}
                    @endif
                  </div>
                </a>
                <a id="edit-action-save" class="pull-right padding-5 km-btn-load">
                  <div class="btn textbox-30 btn-primary">
                    @if( $bread->button_update_save == 'default' || $bread->button_update_save == '')
                      Save
                    @else
                      {{ $bread->button_update_save }}
                    @endif
                  </div>
                </a>
              </div>
            @endif
          </div>  
        </div>
        <!---->
      </div>
    </div>
  @endif 
  
  @if( isset($bread->page_read_include) && !empty($bread->page_read_include))
    @foreach($bread->page_read_include as $include )
      @include( $include )
    @endforeach
  @endif

  @if( $bread->action_delete && $data)
    <form id="form-bread-destroy" class="hidden" method="POST" action="{{ route($bread->route_destroy, [ 'id' => $data->{$bread->delete_target_col_name} ] ) }}">
    </form>
    <div id="modal-small-confirm" class="modal add-backdrop">
      <div class="modal-dialog row">
        <div class="bg-white col-sm-4 col-sm-offset-4" style="top:50%; transform: translateY(-50%);">
          <div class="text-black height-50">
              <span id="confirm-msg" class="textbox-50 text-center">
                  Are you confirm?
              </span>
          </div>
          <div class="modal-footer height-50 no-padding no-margin row">
                  <div class="col-xs-6 padding-10">
                    <a id="small-confirm-action" class="km-btn-load">
                      <div class="btn textbox-30 btn-danger">
                        Confirm Delete
                      </div>
                    </a>             
                  </div>
                  <div class="col-xs-6 padding-10">
                    <a data-dismiss="modal">
                      <div class="btn textbox-30 text-black border">
                        Cancel
                      </div>
                    </a>             
                  </div>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection


@if( $bread->action_edit )
    @push('scripts')
      <script>
        this_component = $('#{{ $component_id }}');
        this_form = this_component.find('#{{ $form_id }}');

        this_component.find('#edit-button').on('click', enableEdit );
        this_component.find('#edit-action-cancel').on('click', disableEdit );
        this_component.find('#edit-action-save').on('click', saveSubmit );
        
        if( "{{session('form_default_state') }}" == 'edit' ){
          errorEdit();
        }

        function errorEdit(){
          // Replace Old Data
          @if( !empty(session("_old_input")) )
            @foreach( session("_old_input") as $key => $value )
              this_form.find('input[name={{$key}}]').val('{{$value}}');
              this_form.find('select[name={{$key}}]').val('{{$value}}');              
            @endforeach
          @endif
          enableEdit();
        }

        function saveSubmit(){
          this_form.submit();
        }

        function enableEdit(){
          this_component.find('#edit-button').addClass('hidden');
          this_component.find('#edit-action-button').removeClass('hidden');
          this_component.find('.edit-input').prop('readonly', false);
          this_component.find('.edit-input-disabled').prop('disabled', false);
        }

        function disableEdit(){
          this_form.trigger("reset"); // Reset Form
          this_component.find('#edit-button').removeClass('hidden');
          this_component.find('#edit-action-button').addClass('hidden');
          this_component.find('.edit-input').prop('readonly', true);
          this_component.find('.edit-input-disabled').prop('disabled', true);
          $('.alert').addClass('hidden');
        }
      </script>
    @endpush
@endif


@if( $bread->action_delete )
    @push('scripts')
      <script>
        $('#delete-button').on('click', function(){
          @if($bread->action_edit)
          disableEdit();
          @endif
          setMConfirmCallback( deleteSubmit );
          $('#modal-small-confirm').modal('show');
        });
        
        function deleteSubmit(){
          $('#form-bread-destroy').submit();
        }

        function setMConfirmCallback( callback ,param = null , message = "Are you confirm?"){;
          $('#modal-small-confirm #confirm-msg').html(message);
          $('#modal-small-confirm #small-confirm-action').off().on('click', function(){
            if( param != null ) {
                      callback( param );
                  }else{
                      callback();
                  } 

          } );
        }
      </script>
    @endpush
@endif