@php 
  // Type
  // text, textarea, password, radio, checkbox, hidden

  // Property list
  // required, readonly, autocomplete, autofocus, checked, disabled

  // Attribute list
  // placeholder, rows, cols, wrap

  // default value
  if( !isset($type) || $type == null ) $type = "text";
  if( !isset($name) ) $name = "";
  if( !isset($label) ) $label = $name; 
  if( !isset($value) ) $value = "";
  if( !isset($input_class) ) $input_class = "";
  if( !isset($rows) ) $rows = '4';        
  if( !isset($props) ) $props = [];        
  if( !isset($attrs) ) $attrs = [];        
  if( !isset($default_state) ) $default_state = "";
  if( !isset($edible) ) $edible = true;
  if( !isset($no_label) ) $no_label = false;
  if( !isset($container_class) ) $container_class = "padding-5";

  $attr_tags = "";
  $prop_tags = "";
  $star_required = "";
  $edible_class ="";
  
  // Process Attrs
  $match_attrs_array = (object)[
    'text' => ['placeholder','autocomplete','maxlength'],
    'checkbox' => [],
    'radio' => [],
    'image' => ['align', 'alt', 'height', 'width', 'src'],
    'file' => ['accept'],
    'password' => ['placeholder','autocomplete'],
    'email' => ['placeholder','autocomplete','maxlength'],
    'url' => ['placeholder','autocomplete','maxlength'],
    'number' => ['placeholder','max','min','maxlength'],
    'textarea' => ['placeholder','autocomplete', 'rows','cols','wrap','maxlength'],
    'dropdown' => [],
    'hidden' => [],
    'boolean' => [],
  ];
  $match_attrs =  $match_attrs_array->$type;
  $matched_attrs = [];
  $attrs = (object)$attrs;
  $has_autocomplete = false;
  foreach($match_attrs as $key){
    if( isset( $attrs->$key ) ) {
      if($key == "autocomplete") $has_autocomplete = true;
      array_push($matched_attrs, $key."=".$attrs->$key );
    }
  }

  if(!$has_autocomplete){
    array_push($matched_attrs, "autocomplete=off");
  }

  $attr_tags = implode(' ', $matched_attrs);

  // Process Default State
  $edible_disabled  = ['dropdown', 'checkbox', 'radio', 'image', ' file', 'boolean'];
  if( $default_state == 'read' ){
    if( in_array( $type, $edible_disabled ) ){
      array_push($props, 'disabled' );
    }else{
      array_push($props, 'readonly' );
    } 

    //Format ~Edible~ )
    if($edible) {
      if( in_array( $type, $edible_disabled ) ){
        $edible_class = "edit-input-disabled";
      }else{
        $edible_class = "edit-input";
      } 
    }
  }else{
    if(!$edible) {
      if( in_array( $type, $edible_disabled ) ){
        array_push($props, 'disabled' );
      }else{
        array_push($props, 'readonly' );
      } 
    }
  }
  
  // Process Props
  $match_props_array = (object)[
    'text' => ['required','readonly','disabled', 'autofocus'],    
    'checkbox' => ['required', 'disabled','autofocus', 'checked'],
    'radio' => ['required', 'disabled','autofocus', 'checked'],
    'image' => ['required', 'disabled', 'autofocus' ],
    'file' => ['required','disabled', 'autofocus','multiple'],
    'password' => ['required','readonly','disabled', 'autofocus'],
    'email' => ['required','readonly','disabled','autofocus', 'multiple'],
    'url' => ['required','readonly','disabled','autofocus'],
    'number' => ['required','readonly','disabled','autofocus'],
    'textarea' => ['required','readonly','disabled','autofocus'],
    'dropdown' => ['required', 'disabled', 'autofocus', 'multiple'],
    'hidden' => [],
    'boolean' => ['required', 'disabled', 'autofocus', 'multiple'],
  ];
  $match_props = $match_props_array->$type;
  $matched_props = array_intersect( $match_props , $props );
  $matched_props = array_unique($matched_props);
  $prop_tags = implode(' ', $matched_props);

  if( in_array("required", $props ) ){
    $star_required = "*";
  }

  // Format ~Label~
  $label= ucwords( str_replace('_', ' ', $label) );

  // Grid Class
  if( $no_label ){
    $label_grid_class="hidden";
    $input_grid_class="col-xs-12";
  }else{
    $label_grid_class="col-xs-12 col-sm-4";
    $input_grid_class="col-xs-12 col-sm-8";
  }

  // Input Container Class
@endphp

<div class="km-format-input {{ $container_class }}">
@if( $type == 'textarea')
  <div class="row">
    <div class="{{ $label_grid_class }} padding-right-5">
      <div class="textbox-40">
        {{ $label }}
        {{ $star_required }}
      </div>
    </div>
    <div class="{{ $input_grid_class }}" style="margin-top: 2px;">
        <textarea 
          class="line-height-20 form-control {{ $input_class }} {{ $edible_class }}" 
          name="{{ $name }}"
          {{ $attr_tags }}
          {{ $prop_tags }}
        >{{ $value }}</textarea>
    </div>
  </div>
@elseif( $type == 'hidden' )
  <input  type="hidden" 
          name="{{ $name }}"  
          value="{{ $value }}"
  >
@elseif( $type == 'dropdown' )
  @php
    // Check Options Data
    if( !isset($options) ) $options = [];

    //options: ['data_type','data','no_select', 'no_select_text', 'allow_select_empty'];
    $options = (object)$options;
    $option_data_type = isset($options->data_type)? $options->data_type: 'value_as_key';
    $option_data = isset($options->data)? $options->data: [] ;
    $option_no_select = isset($options->no_select)? $options->no_select: true ;
    $option_no_select_text = isset($options->no_select_text)? $options->no_select_text: "Please select an option.";
    $option_allow_select_empty = isset($options->allow_select_empty)? $options->allow_select_empty: false;
    $has_selected = false;
  @endphp
  <div class="row height-40">
    <div class="{{ $label_grid_class }} padding-right-5">
      <div class="textbox-40">
        {{ $label }} 
        {{ $star_required }}
      </div>
    </div>
    <div class="{{ $input_grid_class }}" style="margin-top: 2px;">
      <select 
        class="line-height-40 form-control {{ $input_class }} {{ $edible_class }}" 
        name="{{ $name }}" 
        {{ $prop_tags }}
      >
        @if($option_no_select)
          <option 
              value="" 
              @if( !$option_allow_select_empty )
                disabled 
              @endif
              @if( $value == ""  )
                  selected
                  @php $has_selected = true; @endphp
              @endif
            >{{ $option_no_select_text }}
          </option>
        @endif

        @if( $option_data_type == 'value_as_key' )
          @foreach( $option_data as $key => $option)
            <option value="{{ $key }}" 
              @if( $value == $key)
                selected
                @php $has_selected = true; @endphp
              @endif
            >{{ $option }}</option> 
          @endforeach
        @endif

        @if( $option_data_type == 'keyname_as_key' )
          @foreach( $option_data as $option)
            @php
            if ( is_array( $option ) ){
                $option = (object)$option;
            }
            @endphp
            <option value="{{ $option->value }}" 
              @if( $value == $option->value )
                selected
                @php $has_selected = true; @endphp
              @endif
            >{{ $option->name }}
            </option> 
          @endforeach
        @endif

        @if( !$has_selected )
          <option selected >Error: Data value not matched with any option value.</option>
        @endif

        @if( $option_data_type != 'value_as_key' && $option_data_type != 'keyname_as_key')
          <option selected >Error: Invalid Options Data Type Format.</option>
        @endif
      </select>
    </div>
  </div>
@elseif( $type == 'boolean' )
  <div class="row height-40">
    <div class="{{ $label_grid_class }} padding-right-5">
      <div class="textbox-40">
        {{ $label }} 
        {{ $star_required }}
      </div>
    </div>
    <div class="{{ $input_grid_class }}" style="margin-top: 2px;">
      <select 
        class="line-height-40 form-control {{ $input_class }} {{ $edible_class }}" 
        name="{{ $name }}" 
        {{ $prop_tags }}
      >
        <option disabled 
            @if( $value == ""  )
                selected
              @endif
          >Please select an option.
        </option>

        @foreach( [ 1 => 'True', 0 => 'False' ] as $key => $option)
          <option value="{{ $key }}" 
            @if( $value == $key)
              selected
            @endif
          >{{ $option }}</option> 
        @endforeach
      </select>
    </div>
  </div>
@else
  <div class="row height-40">
    <div class="{{ $label_grid_class }} padding-right-5">
      <div class="textbox-40">
        {{ $label }}
        {{ $star_required }}
      </div>
    </div>
    <div class="{{ $input_grid_class }}" style="margin-top: 2px;">
        <input  type="{{ $type }}" 
                class="line-height-40 form-control {{ $input_class }} {{ $edible_class }}" 
                name="{{ $name }}"  
                value="{{ $value }}"
                {{ $attr_tags }}
                {{ $prop_tags }}
        >
    </div>
  </div>

@endif
</div>