@extends($bread->base_layout)

@section('subheader')
  <div class="height-50 row border-bottom hidden-print">
    <div class="col-xs-12">
      <div class="pull-left">
        <a class="btn" href="{{$route_back}}">
          <i class="icon-50 material-icons text-black">arrow_back</i>
        </a>
      </div>
    </div>
  </div>
  <div class="height-50 row border-bottom">
    <div class="col-xs-12">
      <div class="textbox-50 text-18 text-black text-left">{{$title}}</div>
    </div>
  </div>
@endsection

@section('content_class','bg-null padding-bottom-100 text-left text-black')
@section('content_height','calc(100vh - 150px)')
@section('content')

  @if( !empty( $bread_error) || session('bread_error') )
    @php
      if( session('bread_error') ) $bread_error = session('bread_error');
    @endphp
    <div class="text-left alert alert-danger no-margin">
      <span class="help-block">
        @foreach($bread_error as $e)
        <strong>{{ $e }}</strong>
        <br>
        @endforeach
      </span>
    </div>
  @endif
  @if( session('bread_success') )
    <div class="text-left alert alert-success no-margin">
      <span class="help-block">
        <strong>{!! session('bread_success') !!}</strong>        
      </span>
    </div>
  @endif
  @php
    // Setup Form Parameter
    $form_id = 'form-km-bread-add';
    $bread_form_action_type = "add";
  @endphp
  <div class="row border-bottom bg-white">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12">
          @if( $bread->action_add && $bread->form)
              @include($bread->form)
          @endif
        </div>
      </div>
      <!-- panel footer -->
      <hr class="no-margin">
      <div class="row height-40">
        <a class="pull-right padding-5 km-btn-load" onclick="$('#{{ $form_id }}').submit();">
          <div class="btn textbox-30 btn-primary">
            @if( $bread->button_add_save == 'default' || $bread->button_add_save == '')
              Save
            @else
              {{ $bread->button_add_save }}
            @endif
          </div>
        </a>
      <div>
    </div>
  </div>

  @if( isset($bread->page_add_include) && !empty($bread->page_add_include))
    @foreach($bread->page_add_include as $include )
      @include( $include )
    @endforeach
  @endif

@endsection


@if( $bread->action_add )
  @push('scripts')
    <script>
      this_form = $('#{{ $form_id }}');

      if( "{{session('form_default_state') }}" == 'add' ){
        errorAdd();
      }

      function errorAdd(){
        // Replace Old Data
        @if( !empty(session("_old_input")) )
          @foreach( session("_old_input") as $key => $value )
            this_form.find('input[name={{$key}}]').val('{{$value}}');
            this_form.find('select[name={{$key}}]').val('{{$value}}');       
          @endforeach
        @endif
      }
    </script>
  @endpush
@endif