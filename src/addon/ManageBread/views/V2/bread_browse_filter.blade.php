
@if( $bread->filter_setting->show_filter )
<form id="form-browse-with-filter" class="padding-5" method="GET" action="{{ route( $bread->route_browse) }}">
	@if( $bread->filter_setting->with_filter_option )
	<div class="row border-bottom padding-bottom-10">
		<div class="text-left">
			<div class="padding-h">
				Filter Option
			</div>

			@foreach( $bread->filter_setting->filter_options as $filter_name => $filter_opt )
			<div class="col-sm-6 col-xs-12">
				@php
					$val = "";
					$filter_inputs = Request::input('filters');
					if( $filter_inputs ){
						if( isset( $filter_inputs[$filter_name] )){
							$val = $filter_inputs[$filter_name];
						}
					}
				@endphp

			    @component('KMBreadView::V2.bread_format_input', [
			                                                'name' => 'filters['.$filter_name.']',
			                                                'label' => $filter_opt->label? : ucwords(str_replace('_',' ', $filter_name)),
			                                                'container_class' => 'padding-right-5',
			                                                'value' => $val,
			                                                'type' => 'dropdown',
			                                                'props' => [],
			                                                'attrs' =>[],
			                                                'options' => [
			                                                  	'data' => $filter_opt->data,
			                                                	'allow_select_empty' => true,
			                                                	'no_select_text' => 'All'
			                                                ],
			                                                'default_state' => 'edit'

			          ])
			    @endcomponent
		    </div>
		    @endforeach
		</div>
	</div>
	@endif
	@if( $bread->filter_setting->with_sort )
		<div class="row border-bottom padding-top-10 padding-bottom-10">
			<div class="text-left">
				@if( $bread->filter_setting->with_sort_option )
			 	<div class="col-sm-6 col-xs-12">
			      @component('KMBreadView::V2.bread_format_input', [
			                                                'name' => 'sort',
			                                                'label' => 'Sort By',
			                                                'container_class' => 'padding-right-5',
			                                                'value' => Request::input('sort') ? : '',
			                                                'type' => 'dropdown',
			                                                'props' => [],
			                                                'attrs' =>[],
			                                                'options' => [
			                                                  'data' => $bread->filter_setting->sort_options,
			                                                ],
			                                                'default_state' => 'edit'

			          ])
			      @endcomponent
			 	</div>
			 	@endif
			 	@if( $bread->filter_setting->with_sort_order )
			 	<div class="col-sm-6 col-xs-12">
				    @component('KMBreadView::V2.bread_format_input', [
				                                              'name' => 'sort_order',
				                                              'label' => 'Sort Order',
				                                              'container_class' => 'padding-right-5',
				                                              'value' => Request::input('sort_order') ? : 'asc',
				                                              'type' => 'dropdown',
				                                              'props' => [],
				                                              'attrs' =>[],
				                                              'options' => [
				                                                'data' => ['asc' => 'ASC', 'desc' => 'DESC' ],
				                                                'no_select' => false
				                                              ],
				                                              'default_state' => 'edit'

				        ])
				    @endcomponent
			  	</div>
			  	@endif		
			</div>
		</div>
	@endif

	<div class="row padding-top-10 padding-h">
		<div class="pull-right">
			<button class="btn btn-primary padding-5">Apply Filters</button>
		</div>
		<div class="pull-right margin-right">
			<a href="{{ route( $bread->route_browse) }}" class="btn btn-default padding-5">Remove Filters</a>
		</div>
	</div>
</form>
@endif

@push('scripts')
	<script type="text/javascript">
		
		$(function()
		{
		    $("#form-browse-with-filter").submit(function()
		    {
		        $(this).find('select option:selected[value=""]').attr("disabled", "disabled");

		        return true; // ensure form still submits
		    });
		});
	</script>
@endpush