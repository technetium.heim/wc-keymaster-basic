@if( isset($data->isActivated) )
  @if( !$data->isActivated )
    <div class="col-xs-12">
      <!-- panel -->
      <div class="row margin-10 border bg-white">
        <div class="col-xs-12">
          <!-- panel header -->
          <div class="row border-bottom height-40">
            <div class="col-xs-6">
              <div class="textbox-40">Registration In Progress</div>
            </div>
          </div>
          <!-- panel body -->
          <div class="row">
              <div class="textbox-40">
                Click button below to resend Register Token Email to <strong>{{$data->email }}</strong>. 
              </div>
            </div>
          </div>
          <!-- panel footer -->
          <hr class="no-margin">
          <form id="form-resend-register-token" class="hidden" method="POST" action="{{ route($data->resend_route) }}">
              <input type="hidden" name="email" value="{{ $data->email }}">
          </form>
          <div class="row">
            <a class="pull-left padding-10 km-btn-load" onclick="$('#form-resend-register-token').submit();">
                <div class="btn textbox-30 btn-primary">
                  Resend Register Token
                </div>
              </a>         
          </div>
        </div>  
      </div>
      <!---->
    </div>
  @endif
@endif