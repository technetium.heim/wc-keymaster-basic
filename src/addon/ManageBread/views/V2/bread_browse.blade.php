@extends($bread->base_layout)

@php
  // Setup Pagination
  $is_paginate = false;
  if( $data instanceof \Illuminate\Pagination\LengthAwarePaginator ){
    $is_paginate = true;
    $paginator = $data;
    $data = $paginator->items();
  }
@endphp

@section('subheader')
  <div class="height-50 row border-bottom hidden-print">
    <div class="col-xs-12">
      <div class="pull-left">
        <a class="btn" href="{{$route_back}}">
          <i class="icon-50 material-icons text-black">dashboard</i>
        </a>
      </div>
      <div class="pull-right">
        <a class="btn" onclick="window.print();">
          <i class="icon-50 material-icons text-black">print</i>
        </a>
      </div>
    </div>
  </div>
  <div class="height-50 row border-bottom">
    <div class="col-xs-6">
      <div class="textbox-50 text-18 text-black text-left">{{$title}}</div>
    </div>
    @if($bread->action_add)
      <div class="col-xs-6">
          <a class="pull-right" href="{{ route($bread->route_add) }}">
            @if( $bread->button_add == 'default' || $bread->button_add == '')
              <i class="btn icon-50 material-icons text-primary">add</i>
            @else
              <div class="padding-5">
                <div class="btn textbox-40 btn-primary">{{ $bread->button_add }}</div>
              </div>
            @endif
          </a>
      </div>
    @endif
  </div>

  @php
    $show_sub_filter = false; 
    if( isset($bread->filter_setting) ){
      if( $bread->filter_setting->show_filter ) $show_sub_filter = true;
    }
  @endphp

  <div class="height-40 row border-bottom">
    <div class="col-xs-10"> 
      <div class="textbox-40 text-11 text-sub text-left">
        @if($is_paginate)
          Show {{ $paginator->firstItem() }} - {{ $paginator->lastItem() }} of total {{ $paginator->total() }} results.
        @else
          Total {{count( (array)$data)}} results.
        @endif

        {{-- FILTER MESSAGE --}}
        @if( $show_sub_filter )
          @if( $bread->filter_setting->with_filter_option )
            @php
              $filter_msg = '';
              $filter_inputs =  Request::input('filters');
              if( $filter_inputs ){
                $filter_msg = 'Filtered by: ';

                foreach( $filter_inputs as $filter_name => $filter_value ){
                  if( $filter_value != "" ){
                    $label = $bread->filter_setting->filter_options->$filter_name->label? : ucwords(str_replace('_',' ', $filter_name));

                    $show_value = $bread->filter_setting->filter_options->$filter_name->data->$filter_value;
                    $filter_msg .= $label.' = '.$show_value.', ';
                  }
                }


                $filter_msg = trim( $filter_msg , ' ,' );
                $filter_msg .= '.';
              }
            @endphp

            {{ $filter_msg }}
          @endif

          @if(  $bread->filter_setting->with_sort )
            @php
              $sort_msg = '';
              if( Request::input('sort') || Request::input('sort_order') ){
                $sort_msg = 'Sorted by: ';
              
                if( Request::input('sort') ) {
                  $sort_msg .= $bread->filter_setting->sort_options[Request::input('sort')] ;
                  $sort_msg .= Request::input('sort_order')? ', ': '';
                }

                if( Request::input('sort_order') ) {
                  $sort_msg .= strtoupper(Request::input('sort_order'));
                }

                $sort_msg .= '.';
              }
            @endphp

            {{ $sort_msg }}
          @endif
        @endif
      </div>
    </div>
    
    <div class="col-xs-2"> 
      {{-- FILTER & SORT BUTTON --}}
      @if( $show_sub_filter )
          <div class="pull-right">
            <a class="btn" data-toggle="collapse" href="#bread-filter-view">
              <i class="icon-40 material-icons">filter_list</i>
            </a>
          </div>
      @endif
      {{-- END: FILTER & SORT BUTTON --}}
    </div>
  </div>
@endsection

@section('content_class','padding-bottom-100')
@section('content_height','calc(100vh - 220px)')
@section('content')
    {{-- ADDON FOR SORT AND FILTER--}}
    @if($show_sub_filter)
      <div class="browse-addon-options collapse border-bottom" id="bread-filter-view">
        <div class="row">
          @if( $bread->filter_setting->show_filter )
            @include( $bread->filter_setting->filter_view )
          @endif
        </div>
      </div>
    @endif
    {{-- END: ADDON FOR SORT AND FILTER--}}
    {{-- Browse Table Head --}}
    @if(count( (array)$data) > 0)
      <div class="height-30 row text-left text-sub bg-white border-bottom">
        <div class="col-xs-12">
          <div class="row">
            @foreach($bread->browse_cols as $index => $col)
                <div class="{{ $bread->cols_class[$index] }}">
                  <div class="textbox-30">{{ ucwords( str_replace('_', ' ', $col) ) }}</div>
                </div>
            @endforeach
          </div>
        </div>
      </div>
    @endif
    {{-- END: Browse Table Head --}}
    <div class="row">
        <div class="col-xs-12">
          @if(count( (array)$data) > 0)
            @foreach($data as $item)
              <div class="row">
                <div class="col-xs-12">
                  <a href="{{$item->route_read}}">
                    <div class="row text-left text-primary bg-white border-bottom">
                      <div class="pull-left width-full">
                        <div class="row">
                            @foreach($bread->browse_cols as $index => $col)
                                <div class="{{ $bread->cols_class[$index] }}">
                                    <div class="textbox-30">
                                        @if( isset($item->$col) )
                                            {{ $item->$col }}
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            @endforeach
          @else
            @if( !empty( $bread_error) )
              <div class="text-left alert alert-danger">
                <span class="help-block">
                  @foreach($bread_error as $e)
                  <strong>{{ $e }}</strong>
                  @endforeach
                </span>
              </div>
            @else
              <div class="margin-lg-top">
                <img width="100" src="{{asset('images/empty.png')}}"/>
                <div class="text-null">Oops, it is empty.</div>
              </div>
            @endif
          @endif
        </div>
    </div>

    @if( $is_paginate )
      @php
        if( Request::input('sort') ){
          $paginator->appends(['sort' => Request::input('sort')]);
        }

        if( Request::input('sort_order') ){
          $paginator->appends(['sort_order' => Request::input('sort_order')]);
        }

        if( Request::input('filters') ){
          $paginator->appends(['filters' => Request::input('filters')]);
        }
      @endphp
     
      {{ $paginator->links() }}

    @endif
    <div class="height-100"></div>
@endsection

@push('scripts')
@endpush