{{-- 

This is Bread Form 
--}}
@php
    // Setup Edit Parameter
    $bread_form_id = isset($form_id)? $form_id: "form-km-bread-input";
    $bread_form_action_type = isset($bread_form_action_type)? $bread_form_action_type: "";

    if( $bread_form_action_type == 'read'){
        $bread_form_route = isset( $bread->route_update ) && isset($data->{$bread->edit_target_col_name})? route($bread->route_update, ['id' => $data->{$bread->edit_target_col_name} ]) : "";
        $bread_form_show_cols = $bread->read_cols;
        $bread_form_hidden_cols = $bread->edit_cols_hidden;
        $bread_form_locked_cols = $bread->edit_cols_locked;
        $bread_form_values = $data;
        $bread_default_state = 'read';
        $bread_print_form = true;
    }elseif($bread_form_action_type == 'add' ){
        $bread_form_route = isset( $bread->route_save )? route($bread->route_save) : "";
        $bread_form_show_cols = $bread->add_cols;
        $bread_form_hidden_cols = $bread->add_cols_hidden;
        $bread_form_locked_cols = $bread->add_cols_locked;
        $bread_form_values = $data;
        $bread_default_state = "add";
        $bread_print_form = true;
    }else{
        $bread_print_form = false;
    }


@endphp

@if($bread_print_form)
<form id="{{ $bread_form_id }}" method="POST" action="{{ $bread_form_route }}">
  @foreach($bread_form_hidden_cols as $col)
    @component('KMBreadView::V2.bread_format_input', [
                                                'name' => $col,
                                                'value' => isset($bread_form_values->$col) ? $bread_form_values->$col: "" ,
                                                'type' => 'hidden'
    ])
    @endcomponent
  @endforeach
  @foreach($bread_form_show_cols as $col)
    @component('KMBreadView::V2.bread_format_input', [
                                                'name' => $col,
                                                'value' => isset($bread_form_values->$col) ? $bread_form_values->$col: "" ,
                                                'type' => isset($bread->inputs->$col->type)? $bread->inputs->$col->type: "",
                                                'props' => isset($bread->inputs->$col->props) ? $bread->inputs->$col->props:[],
                                                'attrs' => isset($bread->inputs->$col->attrs) ? $bread->inputs->$col->attrs:[],
                                                'options' => isset($bread->inputs->$col->options) ? $bread->inputs->$col->options:[],
                                                'edible' => !in_array($col, $bread_form_locked_cols ),
                                                'default_state' => $bread_default_state,

    ])
    @endcomponent
  @endforeach
</form>
@else
    Error: Form Action Type Not Set
@endif




{{--

js callback
- add
- reload value
-



--}}
