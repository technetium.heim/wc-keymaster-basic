@push('scripts')
  <script>
    function buttonLoading( button_selector ){
      button_selector.addClass('btn disabled');
      content = button_selector.children().clone();
      content.addClass('loading-icon')
      content.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
      button_selector.children().addClass('hidden');
      button_selector.append(content);
    }

    function buttonLoadingCancel( button_selector ){
      button_selector.removeClass('disabled');
      button_selector.children().removeClass('hidden');
      button_selector.find('.loading-icon').remove();
    }

    $('.km-btn-load').on('click', function(){
      buttonLoading( $(this) );
    });
  </script>
@endpush