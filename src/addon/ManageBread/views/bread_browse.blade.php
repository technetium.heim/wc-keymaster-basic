@php
    $table_title = $bread->table_name;
    $cols = $bread->table_cols;

    $route_browse = $bread->route_browse;
    $route_read = $bread->route_read;
@endphp

<div>                 
    @component('WCView::general.components.panels.main')
    @slot('title')
        {{--<!-- Header -->--}}
       <div>
            <span>{{ $table_title }}</span>
       </div>
       
    @endslot
    @slot('content')
        {{--<!-- Table -->--}}
         <table id="bootgrid" class="table table-condensed table-hover table-striped">
            <thead>
                <tr> 
                    @foreach ($cols as $column) 
                        @if ($column == 'id')
                            <th data-column-id="id" data-type="numeric" data-order="asc">ID</th>
                        @else
                            <th data-column-id="{{ $column }}" data-formatter="text">{{ ucwords($column) }}</th>
                        @endif
                    @endforeach
                    <th data-column-id="actions" data-formatter="actions" data-sortable="false" data-width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    @endslot
    @endcomponent     
</div>


<script type="text/javascript">
$(document).ready(function() {
   runBootgrid();
});

    function runBootgrid(){
        var bootgrid = $("#bootgrid").bootgrid({
        ajax: true,
        url: "{!!  $route_browse !!}",
        ajaxSettings: {
            method: "GET",
        },
        caseSensitive: false,
        columnSelection: false,
        // responseHandler: function (response) { alert(JSON.stringify(response)); return response; },
        formatters: {
                'actions':function(column,row) {
                var button="";
                // alert(JSON.stringify(row));
                @if($bread->action_read == true)
                button += '<a class="btn btn-sm" href="{!! $route_read !!}/'+row.id+'"><span class="fa fa-fw fa-external-link"></span></a>'
                @endif
                @if($bread->action_delete == true)
                button += '<a class="btn btn-sm action-delete" data-toggle="modal" data-target="#modal-bread-delete" data-id="'+row.id+'"  data-name="'+row.name+'"><span class="fa fa-fw fa-trash-o"></span></a>'
                @endif  
                return button;   
            }
        },
        css: {
            header: "bootgrid-header",
            footer: "full-height"
        }
    }).on("loaded.rs.jquery.bootgrid", function() {
 
    });
    }
</script>