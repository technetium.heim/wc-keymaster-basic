<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRememberTokenToRemoteAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remote_auth', function (Blueprint $table) {
            $table->string('remember_token',100)->nullable()->after('remote_session_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remote_auth', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });
    }
}
