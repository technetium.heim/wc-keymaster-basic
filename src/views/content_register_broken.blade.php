<!-- BOC: form -->
	<form id="form" method="POST">
		{{ csrf_field() }}
		<div class="padding-h">
			<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
				<div>
					<!-- BOC: subtitle -->
						<div class="text-uppercase text-18 text-bold text-black line-height-50 margin-bottom">Broken Link</div>
					<!-- EOC -->
					<!-- BOC: alert -->
					<!-- EOC -->
					<!-- BOC: instruction -->
						<div class="row">
							<div class="col-xs-12">
								<div class="textbox-50 text-black">Please contact admin.</div>
							</div>
						</div>
					<!-- EOC -->
					<!-- BOC: hidden input -->
					<!-- EOC -->
					<!-- BOC: input -->
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class="btn btn-block btn-link margin-bottom line-height-40 height-40" 
							href="{{ route($group_key.'.user.landing') }}"
						>Back to Login</a>
					<!-- EOC -->
				</div>
			</div>	
		</div>
	</form>
<!-- EOC -->