<!-- BOC: form -->
	<form id="form" method="POST" action="{{route($group_key.'.user.register')}}">
		{{ csrf_field() }}
		<div class="padding-h">
			<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
				<div>
					<!-- BOC: subtitle -->
						<div class="text-uppercase text-18 text-bold text-black line-height-50 margin-bottom">Join Us</div>
					<!-- EOC -->
					<!-- BOC: alert -->
						@if($errors->count() > 0)
							<div class="text-left alert alert-danger">
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
							</div>
						@endif
						@if(session()->has('message'))
							<div class="text-left alert alert-success">
								<span class="help-block">
									<strong>{{ session()->get('message') }}</strong>
								</span>
							</div>
						@endif
					<!-- EOC -->
					<!-- BOC: hidden input -->
					<input type="hidden" name="register_token" value="{{ $token }}">
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='text'
							placeholder='Email Address' 
							name='email'
							value="{{old('email') ?: $email}}"
							required
							readonly 
						/>
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='text'
							placeholder='Name' 
							name='name'
							value="{{old('name')}}"
							required
						/>
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='password'
							placeholder='Password' 
							name='password'
							required
						/>
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='password'
							placeholder='Confirm Password' 
							name='password_confirmation'
							required
						/>
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class='btn btn-block btn-primary margin-bottom line-height-50 height-50' 
							onclick="$('#form').submit()"
						>Confirm</a>
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class="btn btn-block btn-link margin-bottom line-height-40 height-40" 
							href="{{ route($group_key.'.user.landing') }}"
						>Cancel</a>
					<!-- EOC -->
				</div>
			</div>	
		</div>
	</form>
<!-- EOC -->