<div class="width-full padding">
	<!-- BOC: hidden form -->
		<form class="hidden" id="form" role="form" method="POST" action="{{ route($group_key.'.user.logout') }}">
			{{ csrf_field() }}
		</form>
	<!-- EOC -->
	<!-- BOC: info -->
		<div class="max-width-xs border no-border-bottom bg-white margin-bottom">
			<div class="border-bottom">
				<div class="line-height-50 height-50">{{ Auth::guard('web_remote')->user()->email }}</div>
			</div>
		</div>
	<!-- EOC -->
	<!-- BOC: action -->
		<div class="max-width-xs border no-border-bottom bg-white margin-bottom">
			<div class="border-bottom">
				<a 
					class="btn btn-block btn-link line-height-50 height-50 disabled"
				>Change Email Address</a>
			</div>
			<div class="border-bottom">
				<a 
					class="btn btn-block btn-link line-height-50 height-50"
					href="{{ route($group_key.'.user.my.account.password') }}"
				>Change Password</a>
			</div>
			<div class="border-bottom">
				<a 
					class="btn btn-block btn-link line-height-50 height-50"
					onclick="$('#form').submit();" 
				>Log Out</a>
			</div>
		</div>
	<!-- EOC -->
</div>