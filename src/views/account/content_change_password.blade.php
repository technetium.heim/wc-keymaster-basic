<!-- BOC: form -->
	<form id="form" method="POST" action="{{ route($group_key.'.user.password.change') }}">
		{{ csrf_field() }}
		<div class="padding">
			<div class='rounded-xs bg-white max-width-xs padding margin-bottom border'>
				<div>
					<!-- BOC: alert -->
						@if($errors->count() > 0)
							<div class="text-left alert alert-danger">
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
							</div>
						@endif
						@if(session()->has('message'))
							<div class="text-left alert alert-success">
								<span class="help-block">
									<strong>{{ session()->get('message') }}</strong>
								</span>
							</div>
						@endif
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='password'
							placeholder='Current Password' 
							name='old_password'
							required
						/>
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='password'
							placeholder='New Password' 
							name='password'
							required
						/>
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='password'
							placeholder='New Password Confirmation' 
							name='password_confirmation'
							required
						/>
					<!-- BOC: button -->
						<a 
							class='btn btn-block btn-primary margin-bottom line-height-50 height-50' 
							onclick="$('#form').submit()"
						>Confirm Update</a>
						<a 
							class='btn btn-block btn-link margin-bottom line-height-50 height-50'
							href="{{ route($group_key.'.user.my.account') }}"
						>Cancel</a>
					<!-- EOC -->
				</div>
			</div>	
		</div>
	</form>
<!-- EOC -->