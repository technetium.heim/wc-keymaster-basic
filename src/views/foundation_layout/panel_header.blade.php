<div class='bg-black'>
  <div class='row text-left text-white line-height-50'>
    <div class='col-xs-9'>
      <a class="wc-btn wc-btn-icon text-white" title="Show Menu" onclick="toggleColLeft();"><i class="fa fa-fw fa-lg fa-bars"></i></a>
      <span class="text-b" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 1.2em;">{{ config('app.name') }}</span>
    </div>
    <div class='col-xs-3'>
      {{--
      <span class="pull-right padding-right">
        <form id="guard-logout" method="POST" action="{{ route($group_key.'.user.logout') }}">
          <a class="text-white" onclick="$('#guard-logout').submit()">
              <span>Logout</span>
          </a>
        </form>
      </span>
      --}}
      <span class="pull-right padding-right">
        <a href="{{ route($group_key.'.user.my.account') }}" class="text-white" >
            <span>My Account</span>
        </a>
      </span>
    </div>
  </div>
</div>