<!-- BOC: form -->
	<form id="form" method="POST" action="{{ route( $group_key.'.user.request.reset') }}">
		{{ csrf_field() }}
		<div class="padding-h">
			<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
				<div>
					<!-- BOC: subtitle -->
						<div class="text-18 text-bold text-black line-height-50 margin-bottom">Forgot your password?</div>
					<!-- EOC -->
					<!-- BOC: alert -->
						@if ($errors->count() > 0)
							<div class="text-left alert alert-danger">
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
							</div>
						@endif
						@if (session()->has('message'))
							<div class="text-left alert alert-success">
								<span class="help-block">
									<strong>{{ session()->get('message') }}</strong>
								</span>
							</div>
						@endif
					<!-- EOC -->
					<!-- BOC: hint -->
						<div class="text-sub text-left margin-md-bottom">Don't worry. Enter your email address. You will receive an email with a link to reset your password.</div>
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='text'
							placeholder='Email Address' 
							name='email'
							value="{{ old('email') }}"
							required
						/>
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class='btn btn-block btn-primary margin-bottom line-height-50 height-50' 
							onclick="$('#form').submit()"
						>Send Reset Password Link</a>
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class="btn btn-block btn-link margin-bottom line-height-40 height-40" 
							href="{{ route( $group_key.'.user.landing') }}"
						>Back to Log In</a>
					<!-- EOC -->
				</div>
			</div>	
		</div>
	</form>
<!-- EOC -->