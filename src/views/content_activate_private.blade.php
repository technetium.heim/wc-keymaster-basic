<form method="POST" action="{{ route( $group_key.'.user.activate') }}">
	 {{ csrf_field() }}
	<div class='margin-bottom text-white text-lg'>
		<span><i class='fa fa-fw fa-shield fa-lg'></i>  {{ config('app.name') }}</span>
	</div>
	<div class='margin-bottom text-white text-b'>Activate Account</div>
	<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
		<div>
			@if ($errors->count() > 0)
				<div class="text-left alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first() }}</strong>
					</span>
				</div>
			@endif
			@if (session()->has('message'))
				<div class="text-left alert alert-success">
					<span class="help-block">
						<strong>{{ session()->get('message') }}</strong>
					</span>
				</div>
			@endif

			<input type="hidden" name="register_token" value="{{ $token }}">

			<div class='margin-bottom'>
				<input 
					class='form-control' 
					type='text'
					placeholder='Email Address' 
					name='email'
					value="{{ $email }}"
					required
					readonly 
				/>
			</div>
			<div class='margin-bottom'>
				<input 
					class='form-control' 
					type='password'
					placeholder='Password' 
					name='password'
					required
				/>
			</div>
			<div class='margin-bottom'>
				<button class='btn btn-block btn-success no-border' 
				>Activate</button>
			</div>
		</div>
		<div>
			<a href="{{ route( $group_key.'.user.landing') }}">Back to Login</a>							
		</div>
	</div>
</form>