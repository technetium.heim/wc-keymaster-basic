@if( !config('keymaster.remote_auth.'.$group_key.'.public') )
	
	@if( $load_page == 'activate')
		@include('KMView::content_activate_private')
	@elseif( $load_page == 'register' )
		@include('KMView::content_register_private')
	@else
		@include('KMView::content_register_broken')
	@endif
	
@else
	
	@if( $load_page == 'verify')
		@include('KMView::content_register_public_verify')
	@elseif( $load_page == 'activate' )
		@include('KMView::content_activate_public')
	@elseif( $load_page == 'register' )
		@include('KMView::content_register_public')
	@else 
		@include('KMView::content_register_public_verify')
	@endif
@endif


