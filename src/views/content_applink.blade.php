<form method="POST" action="{{ route($group_key.'.app.applink') }}">
	 {{ csrf_field() }}
	<div class='margin-bottom text-white text-lg'>
		<span><i class='fa fa-fw fa-shield fa-lg'></i>{{ config('app.name') }}</span>
	</div>
	<div class='margin-bottom text-white text-b'>Linking Application</div>
	<div class='rounded-xs bg-white max-width-sm padding margin-bottom'>
		<div>
			@if ($errors->count() > 0)
				<div class="text-left alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first() }}</strong>
					</span>
				</div>
			@endif
			@if (session()->has('message'))
				<div class="text-left alert alert-success">
					<span class="help-block">
						<strong>{{ session()->get('message') }}</strong>
					</span>
				</div>
			@endif
			<div class='margin-bottom'>
				<input 
					class='form-control' 
					type='text'
					name='app_name' 
					value="{{ config('app.name') }}"
					required
					readonly
				/>
			</div>
			<div class='margin-bottom'>
				<input 
					class='form-control' 
					type='text'
					name="base_url"
					value="{{ url('/') }}"
					required
					readonly
				/>
			</div>
			<div class='margin-bottom'>
				<input 
					class='form-control' 
					type='text'
					name='role_key' 
					placeholder="Role Key (Important) - Example: wooyoo-manage" 
					value="{{ old('role_key') }}"
					required
				/>
			</div>
			<div class="text-left">
				<input type="checkbox" class="form-check-input" id="exampleCheck1" name="public">
				<label class="form-check-label" for="exampleCheck1">Public - [Checked: Not required Register Token to register.]</label>
			</div>
			<div class='margin-bottom'>
				<button class='btn btn-block btn-success no-border' 
				>Create</button>
			</div>
		</div>
		<div>
			<a href="{{ route( $group_key.'.user.landing') }}">Back to Landing</a>							
		</div>
	</div>
</form>