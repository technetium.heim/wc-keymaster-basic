<!-- BOC: form -->
	<form id="form" method="POST" action="{{ route( $group_key.'.user.login') }}">
		{{ csrf_field() }}
		<div class="padding-h">
			<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
				<div>
					<!-- BOC: subtitle -->
						<div class="text-uppercase text-18 text-bold text-black line-height-50 margin-bottom">Log In</div>
					<!-- EOC -->
					<!-- BOC: alert -->
						@if ($errors->count() > 0)
							<div class="text-left alert alert-danger">
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
							</div>
						@endif
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='text'
							placeholder='Email Address' 
							name='email'
							value="{{ old('email') }}"
							required
						/>
					<!-- EOC -->
					<!-- BOC: input -->
						<input 
							class='form-control margin-bottom line-height-50 height-50' 
							type='password'
							placeholder='Password' 
							name='password'
							required
						/>
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class='btn btn-block btn-primary margin-bottom line-height-50 height-50' 
							onclick="$('#form').submit()"
						>Log In</a>
					<!-- EOC -->
					<!-- BOC: button -->
						<a 
							class="btn btn-block btn-link margin-bottom line-height-40 height-40" 
							href="{{ route($group_key.'.user.forgot.index') }}"
						>Forgot Password</a>
					<!-- EOC -->
				</div>
			</div>	
		</div>
	</form>
<!-- EOC -->
<!-- BOC: button -->
	@if( config('keymaster.remote_auth.'.$group_key.'.public') )
		<a 
			class="margin-bottom line-height-40 height-40 text-white" 
			href="{{ route($group_key.'.user.register.index') }}"
		>Sign Up</a>
	@endif
<!-- EOC -->