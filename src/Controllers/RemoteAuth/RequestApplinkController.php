<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RAApplinkTrait;

class RequestApplinkController extends CradleController
{
  use RAApplinkTrait;
}
