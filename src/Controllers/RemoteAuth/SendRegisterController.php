<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RASendRegisterTrait;

class SendRegisterController extends CradleController
{
  use RASendRegisterTrait;
}
