<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RASendActivateTrait;

class SendActivateController extends CradleController
{
  use RASendActivateTrait;
}
