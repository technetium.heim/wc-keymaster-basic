<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RASendChangePasswordTrait;

class SendChangePasswordController extends CradleController
{
  use RASendChangePasswordTrait; 
}
