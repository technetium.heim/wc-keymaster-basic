<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RASendResetPasswordTrait;

class SendResetPasswordController extends CradleController
{
  use RASendResetPasswordTrait; 
}
