<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RASendLoginTrait;

class SendLoginController extends CradleController
{
  use RASendLoginTrait;
}
