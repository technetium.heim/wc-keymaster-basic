<?php

namespace Keymaster\Controllers\RemoteAuth;

use App\Http\Controllers\CradleController;
use Keymaster\foundation\RAPageTrait;
/*
  This Controller is only for simple auth page (view).
  The only method is Logout. 
*/
class AuthController extends CradleController
{
  use RAPageTrait;

}
