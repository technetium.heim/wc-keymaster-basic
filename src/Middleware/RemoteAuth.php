<?php

namespace Keymaster\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Keymaster\foundation\RAUserGroupTrait;

class RemoteAuth
{
    use RAUserGroupTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $status)
    {
        if($status == 'in'){
            // if not login will redirect
            if( !Auth::guard('web_remote')->check()  ){
                return redirect()->route( $this->auth_out_redirect() );
            }
        }

        if($status == 'out'){
            // if logged in will redirect
            if( Auth::guard('web_remote')->check()  ){
                return redirect()->route( $this->auth_in_redirect() );
            }
        }

        return $next($request);
    }
}
