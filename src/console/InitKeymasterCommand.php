<?php 
namespace Keymaster\console;

use Illuminate\Console\Command;
use Artisan;
use Illuminate\Support\Facades\File;

class InitKeymasterCommand extends Command {


    protected $name = 'keymaster:init';
    protected $description = 'Initilize Keymaster (config + user view)';

    public function __construct()
    {
        parent::__construct();
    }


    public function fire()
    {
        if ($this->confirm('Do you wish to Init Keymaster (publish config files) ?')) { 
            Artisan::call('vendor:publish', [
                '--tag' => 'keymaster-config',
            ]);

        
            $this->info('Config file is published.'."\n");
            $this->info('Please run "php artisan keymaster:new-auth" to create New User Group');
        }
    }


}