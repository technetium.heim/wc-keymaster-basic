<?php 
namespace Keymaster\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Illuminate\Support\Facades\File;
use League\Flysystem\MountManager;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\Adapter\Local as LocalAdapter;

class CreateUserGroupCommand extends Command {

    protected $force = true;
    protected $name = 'keymaster:new-auth';
    protected $description = 'Make new Remote Auth User Resource';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }


    public function fire()
    {
        $group_key = $this->ask('Please Enter User Group Key. [default value: primary / secondary]'); 
        $dir = config('keymaster.remote_auth.'.$group_key.'.user_dir');
        $path = resource_path('/views/'.$dir);

        if( $dir !=""){
            if ( File::isDirectory( $path ) ) {
                $this->info( $dir. " folder is already exist @resource/views");
                if ($this->confirm('Continue to replace file in '.$dir.' folder?')){
                    // Run for copy
                    $this->publishTag('keymaster-auth', $dir );
                    $this->info( $dir." folder is replaced.");
                }
            }else{
                $this->publishTag('keymaster-auth', $dir );
                $this->info( $dir." folder is published.");
            }
        }else{
            $this->info("Please enter a value for @config > keymaster.php > keymaster > ".$group_key." > user_dir!");
        }    
    }

    protected function publishTag($tag, $add_dir)
    {
        foreach ($this->pathsToPublish($tag) as $from => $to) {
            // change to dir
            $to = $to.'/'.$add_dir;

            $this->publishItem($from, $to);
        }

        


        $this->info('Publishing complete.');
    }

    /**
     * Get all of the paths to publish.
     *
     * @param  string  $tag
     * @return array
     */
    protected function pathsToPublish($tag)
    {
        return ServiceProvider::pathsToPublish(
            "", $tag
        );
    }

    /**
     * Publish the given item from and to the given location.
     *
     * @param  string  $from
     * @param  string  $to
     * @return void
     */
    protected function publishItem($from, $to)
    {
        if ($this->files->isFile($from)) {
            return $this->publishFile($from, $to);
        } elseif ($this->files->isDirectory($from)) {
            return $this->publishDirectory($from, $to);
        }

        $this->error("Can't locate path: <{$from}>");
    }

    /**
     * Publish the file to the given path.
     *
     * @param  string  $from
     * @param  string  $to
     * @return void
     */
    protected function publishFile($from, $to)
    {
        if (! $this->files->exists($to) || $this->force ) {
            $this->createParentDirectory(dirname($to));

            $this->files->copy($from, $to);

            $this->status($from, $to, 'File');
        }
    }

    /**
     * Publish the directory to the given directory.
     *
     * @param  string  $from
     * @param  string  $to
     * @return void
     */
    protected function publishDirectory($from, $to)
    {
        $this->moveManagedFiles(new MountManager([
            'from' => new Flysystem(new LocalAdapter($from)),
            'to' => new Flysystem(new LocalAdapter($to)),
        ]));

        $this->status($from, $to, 'Directory');
    }

    /**
     * Move all the files in the given MountManager.
     *
     * @param  \League\Flysystem\MountManager  $manager
     * @return void
     */
    protected function moveManagedFiles($manager)
    {
        foreach ($manager->listContents('from://', true) as $file) {
            if ($file['type'] === 'file' && (! $manager->has('to://'.$file['path']) || $this->force )) {
                $manager->put('to://'.$file['path'], $manager->read('from://'.$file['path']));
            }
        }
    }

    /**
     * Create the directory to house the published files if needed.
     *
     * @param  string  $directory
     * @return void
     */
    protected function createParentDirectory($directory)
    {
        if (! $this->files->isDirectory($directory)) {
            $this->files->makeDirectory($directory, 0755, true);
        }
    }

    /**
     * Write a status message to the console.
     *
     * @param  string  $from
     * @param  string  $to
     * @param  string  $type
     * @return void
     */
    protected function status($from, $to, $type)
    {
        $from = str_replace(base_path(), '', realpath($from));

        $to = str_replace(base_path(), '', realpath($to));

        $this->line('<info>Copied '.$type.'</info> <comment>['.$from.']</comment> <info>To</info> <comment>['.$to.']</comment>');
    }
}