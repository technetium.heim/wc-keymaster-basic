<?php 
namespace Keymaster\console;

use Illuminate\Console\Command;
use Artisan;
use Illuminate\Support\Facades\File;

class InitManageSlaveCommand extends Command {


    protected $name = 'manage-slave:init';
    protected $description = 'Initilize Manage Slave';

    public function __construct()
    {
        parent::__construct();
    }


    public function fire()
    {
        if ($this->confirm('Do you wish to Init ManageSlave (publish config files) ?')) { 
            Artisan::call('vendor:publish', [
                '--tag' => 'manage-slave-config',
            ]);

            $this->info('Config file is published.'."\n");
        }
    }


}